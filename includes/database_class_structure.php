<?php
require 'password.php';

interface IInsertable {
    public function insert();
}
interface IDeleteable {
    public function delete();
}
interface IChangeable {
    public function change($property, $newValue);
}

function propertyMatchesColumn($tablename, $property) {
	$columns = (new database_connector())->run_query("SHOW COLUMNS FROM " . $tablename, array(), PDO::FETCH_NUM);
	$matchesColumn = false;
	for ($j = 0; $j < sizeof($columns); $j++) {
		if ($property == $columns[$j][0]) {
			$matchesColumn = true;
		}
	}
	return matchesColumn;
}

class student implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $fname;
    private $lname;
    private $n_number;
    private $rfid;
    private $type;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($fname, $lname, $n_number, $rfid, $type) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->fname = $fname;
        $this->lname = $lname;
        $this->n_number = $n_number;
        $this->rfid = $rfid;
        $this->type = $type;
    }
    
    public function __sleep() {
        return array('fname', 'lname', 'n_number', 'rfid', 'type');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->fname, $object->lname, $object->n_number, $object->rfid);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM students WHERE n_number=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM students WHERE n_number=?", array($this->n_number));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO students(fname, lname, n_number, rfid, type) VALUES (?,?,?,?,?)", array($this->fname, $this->lname, $this->n_number, $this->rfid, $this->type));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('students', $property)) {
            $this->connector->run_query("UPDATE students SET " . $property . "=? WHERE n_number=?", array($newValue, $this->n_number));
            // Set the property given to the new value
            $this->$property = $newValue;
            print_r($this);
            echo "UPDATE students SET " . $property . "=" . $newValue . " WHERE n_number=" . $this->n_number;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class instructor implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $a_number;
    private $fname;
    private $lname;
    private $password;
    
    // Non-database properties
    private $connector;
    public $temp_authenticated;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($a_number, $fname, $lname, $password, $hash_password = false) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->a_number = $a_number;
        $this->fname = $fname;
        $this->lname = $lname;
        if ($hash_password) {
        	$password = password_hash($password, PASSWORD_DEFAULT);
        }
        $this->password = $password;
        
        $this->temp_authenticated = false;
    }
    
    public function __sleep() {
        return array('a_number', 'fname', 'lname', 'password', 'temp_authenticated');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->a_number, $object->fname, $object->lname, $object->password);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM instructors WHERE a_number=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM instructors WHERE a_number=?", array($this->a_number));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO instructors(a_number, fname, lname, password) VALUES (?,?,?,?)", array($this->a_number, $this->fname, $this->lname, $this->password));
    }
    
    public function change($property, $newValue, $hash_password = true) {
    	if (property_exists(get_class($this), $property) && propertyMatchesColumn('instructors', $property)) {
        	if ($property == 'password' && $hash_password) {
                $newValue = password_hash($newValue, PASSWORD_DEFAULT);
            }
            $this->connector->run_query("UPDATE instructors SET " . $property . "=? WHERE a_number=?", array($newValue, $this->a_number));
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
    
    public function getHash() {
        return $this->password;
    }
}
class admin implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $username;
    private $fname;
    private $lname;
    private $password;
    private $access_level;
    
    // Non-database properties
    private $connector;
    public $temp_authenticated;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($username, $fname, $lname, $password, $access_level, $hash_password = false) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->username = $username;
        $this->fname = $fname;
        $this->lname = $lname;
        if ($hash_password) {
        	$password = password_hash($password, PASSWORD_DEFAULT);
        }
        $this->password = $password;
        $this->access_level = $access_level;
        
        $this->temp_authenticated = false;
    }
    
    public function __sleep() {
        return array('username', 'fname', 'lname', 'password', 'access_level', 'temp_authenticated');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->username, $object->fname, $object->lname, $object->password, $object->access_level);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM admins WHERE username=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM admins WHERE username=?", array($this->username));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO admins(username, fname, lname, password, access_level) VALUES (?,?,?,?,?)", array($this->username, $this->fname, $this->lname, $this->password, $this->access_level));
    }
    
    public function change($property, $newValue, $hash_password = true) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('admins', $property)) {
            if ($property == 'password' && $hash_password) {
                $newValue = password_hash($newValue, PASSWORD_DEFAULT);
            }
            $this->connector->run_query("UPDATE admins SET " . $property . "=? WHERE username=?", array($newValue, $this->username));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
    
    public function getHash() {
        return $this->password;
    }
}
class class_object implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $class_id;
    private $a_number;
    private $class_name;
    private $late_time;
    private $school_id;
    
    // Non-database properties
    private $connector;
    private $start_time;
    private $end_time;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($class_id, $a_number, $class_name, $late_time, $school_id) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        // initializes object data
        $this->class_id = $class_id;
        $this->a_number = $a_number;
        $this->class_name = $class_name;
        if ($late_time != null) {
            $this->late_time = new DateTime("1970-01-01 " . $late_time, new DateTimeZone("UTC"));
        } else {
            $this->late_time = new DateTime("1970-01-01", new DateTimeZone("UTC"));
        }
        $this->school_id = $school_id;
        
        $associated_periods = $this->connector->run_query("SELECT periods.* FROM periods
                JOIN period_registrations
                WHERE period_registrations.class_id=?
                AND period_registrations.period_id=periods.period_id", array($this->class_id));
        for ($i = 0; $i < sizeof($associated_periods); $i++) {
            $associated_periods[$i] = period::construct_with_array($associated_periods[$i]);
        }
        
        if (sizeof($associated_periods) > 0) {
            $earliest_start_time = null;
            $latest_end_time = null;
            for ($i = 0; $i < sizeof($associated_periods); $i++) {
                if ($earliest_start_time == null || $associated_periods[$i]->start_time->getTimestamp() < $earliest_start_time->getTimestamp()) {
                    $earliest_start_time = $associated_periods[$i]->start_time;
                }
                if ($latest_end_time == null || $associated_periods[$i]->late_time->getTimestamp() > $latest_end_time->getTimestamp()) {
                    $latest_end_time = $associated_periods[$i]->late_time;
                }
            }
            $this->start_time = $earliest_start_time;
            $this->end_time = $latest_end_time;
        } else {
            $this->start_time = null;
            $this->end_time = null;
        }
        
    }
    
    public function __sleep() {
        return array('class_id', 'a_number', 'class_name', 'late_time', 'school_id', 'start_time', 'end_time');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->class_id, $object->a_number, $object->class_name, $object->late_time, $object->school_id);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM classes WHERE class_id=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM classes WHERE class_id=?", array($this->class_id));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO classes(class_id, a_number, class_name, late_time, school_id) VALUES (?,?,?,?,?)", array($this->class_id, $this->a_number, $this->class_name, $this->late_time->format("H:i:s"), $this->school_id));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('classes', $property)) {
            $this->connector->run_query("UPDATE classes SET " . $property . "=? WHERE class_id=?", array($newValue, $this->class_id));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class class_registration implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $registration_id;
    private $class_id;
    private $n_number;
    private $start_date;
    private $end_date;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($registration_id, $class_id, $n_number, $start_date, $end_date) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->registration_id = $registration_id;
        $this->class_id = $class_id;
        $this->n_number = $n_number;
        $this->start_date = new DateTime($start_date, new DateTimeZone("UTC"));
        $this->end_date = new DateTime($end_date, new DateTimeZone("UTC"));
    }
    
    public function __sleep() {
        return array('registration_id', 'class_id', 'n_number', 'start_date', 'end_date');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->registration_id, $object->class_id, $object->n_number, $object->start_date, $object->end_date);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM class_registrations WHERE registration_id=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM class_registrations WHERE registration_id=?", array($this->registration_id));
    }
    
    public function insert() {
    	$this->connector->run_query("INSERT INTO class_registrations(registration_id, class_id, n_number, start_date, end_date) VALUES (?,?,?,?,?)", array($this->registration_id, $this->class_id, $this->n_number, $this->start_date->format("Y-m-d"), $this->end_date->format("Y-m-d")));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('class_registrations', $property)) {
            $this->connector->run_query("UPDATE class_registrations SET " . $property . "=? WHERE registration_id=?", array($newValue, $this->registration_id));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class klockin implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $registration_id;
    private $date;
    private $time_in;
    private $time_out;
    private $late;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($registration_id, $date, $time_in, $time_out, $late) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->registration_id = $registration_id;
        $this->date = new DateTime($date, new DateTimeZone("UTC"));
        $this->time_in = new DateTime($date . $time_in, new DateTimeZone("UTC"));
        if ($time_out != null) {
            $this->time_out = new DateTime($time_out, new DateTimeZone("UTC"));
        } else {
            $this->time_out = $time_out;
        }
        $this->late = $late;
    }
    
    public function __sleep() {
        return array('registration_id', 'date', 'time_in', 'time_out', 'late');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->registration_id, $object->date, $object->time_in, $object->time_out, $object->late);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4]);
        return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM klockins WHERE registration_id=? AND date=? AND time_in=? AND time_out=? AND late=? LIMIT 1", array($this->registration_id, $this->date->format("Y-m-d"), $this->time_in->format('H:i:s'), ($this->time_out == null ? null : $this->time_out->format("H:i:s")), $this->late));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO klockins(registration_id, date, time_in, time_out, late) VALUES (?,?,?,?,?)", array($this->registration_id, $this->date->format("Y-m-d"), $this->time_in->format("H:i:s"), ($this->time_out == null ? null : $this->time_out->format("H:i:s")), $this->late));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('klockins', $property)) {
            $this->connector->run_query("UPDATE klockins SET " . $property . "=? WHERE registration_id=? AND date=? AND time_in=? AND time_out=? AND late=? LIMIT 1", array($newValue, $this->registration_id, $this->date->format("Y-m-d"), $this->time_in->format('H:i:s'), ($this->time_out == null ? null : $this->time_out->format("H:i:s")), $this->late));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class school implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $school_id;
    private $name;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($school_id, $name) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->school_id = $school_id;
        $this->name = $name;
    }
    
    public function __sleep() {
        return array('school_id', 'name');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->school_id, $object->name);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[2]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM schools WHERE school_id=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM schools WHERE school_id=?", array($this->school_id));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO schools(school_id, name) VALUES (?,?)", array($this->school_id, $this->name));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('schools', $property)) {
            $this->connector->run_query("UPDATE schools SET " . $property . "=? WHERE school_id=?", array($newValue, $this->school_id));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class period implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $period_id;
    private $period_name;
    private $school_id;
    private $start_time;
    private $end_time;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($period_id, $period_name, $school_id, $start_time, $end_time) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        // initializes object data
        $this->period_id = $period_id;
        $this->period_name = $period_name;
        $this->school_id = $school_id;
        $this->start_time = new DateTime($start_time, new DateTimeZone("UTC"));
        $this->end_time = new DateTime($end_time, new DateTimeZone("UTC"));
    }
    
    public function __sleep() {
        return array('period_id', 'period_name', 'school_id', 'start_time', 'end_time');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->period_id, $object->period_name, $object->school_id, $object->day, $object->start_time, $object->end_time);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1], $array[2], $array[3], $array[4], $array[5]);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_id($id) {
    	$result = (new database_connector())->run_query("SELECT * FROM periods WHERE period_id=?", array($id));
    	$instance = self::construct_with_array($result[0]);
    	return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM periods WHERE period_id=?", array($this->period_id));
    }
    
    public function insert() {
        $this->connector->run_query("INSERT INTO periods(period_id, period_name, school_id, start_time, end_time) VALUES (?,?,?,?,?)", array($this->period_id, $this->period_name, $this->school_id, $this->start_time->format('H:i:s'), $this->end_time->format('H:i:s')));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('periods', $property)) {
            $this->connector->run_query("UPDATE periods SET " . $property . "=? WHERE period_id=?", array($newValue, $this->period_id));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
class period_registration implements IInsertable, IDeleteable, IChangeable, JsonSerializable {
    private $class_id;
    private $period_id;
    
    // Non-database properties
    private $connector;
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function __construct($class_id, $period_id) {
        // creates the database connector to interact with the database
        $this->connector = new database_connector();
        
        // initializes object data
        $this->class_id = $class_id;
        $this->period_id = $period_id;
    }
    
    public function __sleep() {
        return array('class_id', 'period_id');
    }
    
    public function __wakeup() {
        $this->connector = new database_connector();
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_object($object) {
        $instance = new self($object->class_id, $object->period_id);
        return $instance;
    }
    
    // Shortcut constructor (psuedo-polymorphism)
    public static function construct_with_array($array) {
        $instance = new self($array[0], $array[1]);
        return $instance;
    }
    
    public function delete() {
        $this->connector->run_query("DELETE FROM period_registrations WHERE class_id=? AND period_id=? LIMIT 1", array($this->class_id, $this->period_id));
    }
    
    public function insert() {
        $query = 
        $this->connector->run_query("INSERT INTO period_registrations(class_id, period_id) VALUES (?,?)", array($this->class_id, $this->period_id));
    }
    
    public function change($property, $newValue) {
        if (property_exists(get_class($this), $property) && propertyMatchesColumn('period_registrations', $property)) {
            $this->connector->run_query("UPDATE period_registrations SET " . $property . "=? WHERE class_id=? AND period_id=? LIMIT 1", array($newValue, $this->class_id, $this->period_id));
            // Set the property given to the new value
            $this->$property = $newValue;
        }
    }
    
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
}
?>
<?php	
    include 'includes/standard_strings.php';
    include 'includes/loader.php';
    
    session_start();
    
    $content = '<center><h1>About Klock</h1>
            <img src="images/klock.svg" height=200 />
            </center>
            <h2>Development team</h2>
            <ul>
            <li><b>Teacher:</b> James Hornberger</li>
            <li><b>Project Manager:</b> Logan Lopez</li>
            <li><b>Lead Software Developer:</b> Nate Robb</li>
            <li><b>Development Company:</b> QodeQube</li>
            </ul>
            <h2>License</h2>
            <a href="http://www.gnu.org/licenses/agpl.html">GNU Affero General Public License</a>
            <i><p>All software used in the development of this web application was deemed moral by the Free Software Foundation.</p></i>';
    $headerScripts = '';
    $footerScripts = '';
    $title = 'About';
    
    
    echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$about_menu_item_id, $title);
?>
-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2015 at 07:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `klock`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `username` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `access_level` enum('Default admin','Uber admin') NOT NULL DEFAULT 'Default admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`username`, `fname`, `lname`, `password`, `access_level`) VALUES
('admin', 'The One Ring', '1ToRuleThemAll', '$2y$10$4la8Ls1BNEkQjYjUlMZ9veU5It9HPa0Y5blD2vpXBT6tb3gJwimU.', 'Uber admin');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
`class_id` int(5) unsigned zerofill NOT NULL,
  `a_number` int(6) unsigned zerofill NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `late_time` time DEFAULT NULL,
  `school_id` int(4) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

-- --------------------------------------------------------

--
-- Table structure for table `class_registrations`
--

CREATE TABLE IF NOT EXISTS `class_registrations` (
`registration_id` int(7) unsigned zerofill NOT NULL,
  `class_id` int(5) unsigned zerofill NOT NULL,
  `n_number` int(6) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_registrations`
--


-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE IF NOT EXISTS `instructors` (
  `a_number` int(6) unsigned zerofill NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains user data';

--
-- Dumping data for table `instructors`
--

-- --------------------------------------------------------

--
-- Table structure for table `klockins`
--

CREATE TABLE IF NOT EXISTS `klockins` (
`registration_id` int(7) unsigned zerofill NOT NULL,
  `date` date NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time DEFAULT NULL,
  `late` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klockins`
--

-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE IF NOT EXISTS `periods` (
`period_id` int(5) unsigned zerofill NOT NULL,
  `period_name` varchar(10) NOT NULL,
  `school_id` int(4) unsigned zerofill NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periods`
--

-- --------------------------------------------------------

--
-- Table structure for table `period_registrations`
--

CREATE TABLE IF NOT EXISTS `period_registrations` (
  `class_id` int(5) unsigned zerofill NOT NULL,
  `period_id` int(5) unsigned zerofill DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `period_registrations`
--

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
  `school_id` int(4) unsigned zerofill NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `n_number` int(6) NOT NULL,
  `rfid` varchar(20) DEFAULT NULL,
  `type` enum('Highschool student','Adult student') NOT NULL DEFAULT 'Highschool student'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
 ADD PRIMARY KEY (`class_id`), ADD KEY `a_number` (`a_number`), ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `class_registrations`
--
ALTER TABLE `class_registrations`
 ADD PRIMARY KEY (`registration_id`), ADD KEY `class_id` (`class_id`), ADD KEY `n_number` (`n_number`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
 ADD UNIQUE KEY `a_number` (`a_number`);

--
-- Indexes for table `klockins`
--
ALTER TABLE `klockins`
 ADD KEY `class_registration_id` (`registration_id`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
 ADD PRIMARY KEY (`period_id`), ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `period_registrations`
--
ALTER TABLE `period_registrations`
 ADD KEY `class_id` (`class_id`), ADD KEY `period_id` (`period_id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
 ADD UNIQUE KEY `school_id` (`school_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
 ADD UNIQUE KEY `n_number_2` (`n_number`), ADD UNIQUE KEY `n_number` (`n_number`,`rfid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
MODIFY `class_id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `class_registrations`
--
ALTER TABLE `class_registrations`
MODIFY `registration_id` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `klockins`
--
ALTER TABLE `klockins`
MODIFY `registration_id` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
MODIFY `period_id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
ADD CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`a_number`) REFERENCES `instructors` (`a_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `classes_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `class_registrations`
--
ALTER TABLE `class_registrations`
ADD CONSTRAINT `class_registrations_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `class_registrations_ibfk_2` FOREIGN KEY (`n_number`) REFERENCES `students` (`n_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `klockins`
--
ALTER TABLE `klockins`
ADD CONSTRAINT `klockins_ibfk_2` FOREIGN KEY (`registration_id`) REFERENCES `class_registrations` (`registration_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periods`
--
ALTER TABLE `periods`
ADD CONSTRAINT `periods_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `period_registrations`
--
ALTER TABLE `period_registrations`
ADD CONSTRAINT `period_registrations_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `period_registrations_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `periods` (`period_id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

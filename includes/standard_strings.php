<?php
    class standard_strings
    {
        public static $instructorLogin = 'User Login';
        
        public static $login_noun = 'Login';
        public static $login_verb = 'log in';
        
        public static $studentNumber = 'Student Number';
        public static $studentKlockin = 'Student Klock-in';
        
        public static $klockin_noun = 'Klock-in';
        public static $klockin_verb = 'Klock in';
        
        public static $klockin_field_name = 'klock-in_field';
        
        public static $klockin_menu_item_id = 'klockin_menu_item';
        public static $reports_menu_item_id = 'reports_menu_item';
        public static $manage_menu_item_id = 'manage_menu_item';
        public static $about_menu_item_id = 'about_menu_item';
        
        public static $copyRightCompanyName = 'Qode Qube';
        
        public static $incorrect_username_password = 'wrong username or password';
        public static $incorrect_student_number = '';
        public static $successful_klockin = 'Klocked in';
        public static $error_horrible = 'Something went horribly wrong, please contact your instructor or system administrator';
        public static $error_invalid = 'Invalid value entered';
    }
?>
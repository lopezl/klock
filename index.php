<?php
	include 'includes/standard_strings.php';
	include "includes/loader.php";
	
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <script src="js/jquery.min.js"></script>
    <title>Klock - Instructor login</title>
    
    <!-- Imports css and icon -->
    <link rel="icon" type="image/icon" href="images/favicon.ico">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/form.css" rel="stylesheet">
  </head>
  
  <!-- Body -->
  <body>
    <div class="container">
    
    <!-- The logo -->
      <img class="logoCentered" src="images/klock_with_text.svg" alt="Klock" height="100px">
      
      <!-- The login form -->
      <form class="form" role="form" autocomplete="off" style="margin-top: 50px;" onsubmit="submit_login(); return false;">
        <h1 class="form-heading"><?php echo standard_strings::$instructorLogin; ?></h1>
        <div class="alert alert-danger" id="userAlert" style="display: none;"><strong id="userTitle"></strong><p id="userDescription"></p></div>
        <input type="text" name="username" id="username" class="form-control topInput" placeholder="Username" required autofocus autocomplete="off">
        <input type="password" name="password" id="password" class="form-control bottomInput" placeholder="Password" required autocomplete="off">
        <button class="btn btn-lg btn-primary btn-block" id="submitBtn" type="submit">Sign in</button>
      </form>
      
    </div>
  </body>
  
  <script type="text/javascript">
  var username = document.getElementById("username");
  var password = document.getElementById("password");
  var userAlert = document.getElementById("userAlert");
  var userTitle = document.getElementById("userTitle");
  var userText = document.getElementById("userDescription");
  
function submit_login() {
	var ajaxRequestData = {"username": username.value, "password": password.value};	  
	$.ajax({
		type: "POST",
		url: "api/authenticate/login",
		data: ajaxRequestData,
		dataType: "json",
		success: function(reply) {
			// do different stuff depending on the result returned
			if(reply.data.authenticated){
				if (reply.data.userType == 'admin') {
					window.location.href = "reports.php";
				}
				else if (reply.data.userType == 'instructor') {
					window.location.href = "klockin.php";
				}
			}
			else {
				userTitle.textContent = "Authentication Error";
				userText.textContent = "<?php echo standard_strings::$incorrect_username_password; ?>";
				userAlert.style.display = "";
			}
		},
		error: function(reply) {
			userTitle.textContent = "System error";
			userText.textContent = "backend not responding";
			userAlert.style.display = "";
		},
	});
}
  
  </script>
  
  <?php echo loader::getFooter();?>
</html>

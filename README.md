# Klock #
A PHP based time clock which handles attendance for Sarasota County Technical Institute.

### Technologies used ###
* PHP
* HTML 5 & CSS 3
* Javascript & jQuery
* AJAX
* JSON
* MySQL

### Current status ###
*API being finished, management page being completed*

### Credits ###
* **Project Manager:** Logan Lopez
* **Senior Lead Software Developer:** Nathan Robb
function cloneObject(object) {
	return $.extend(true, {}, object);
}
function defineFunctionIfNotDefined(callback) {
	if (callback === undefined) {
		callback = function() {};
	}
	return callback;
}

var httpReqType_insert = "POST";
var httpReqType_delete = "DELETE";
var httpReqType_change = "PUT";

var dataType_json = "json";

function Student(fname, lname, n_number, rfid, type) {
	this.fname = fname;
	this.lname = lname;
	this.n_number = n_number;
	this.rfid = rfid;
	this.type = type;
	
	this.endpoint = "api/student/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				fname: this.fname,
				lname: this.lname,
				rfid: this.rfid,
				type: this.type
			},
			url: this.endpoint + this.n_number,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.n_number,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.n_number,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function Instructor(a_number, fname, lname, password) {
	this.a_number = a_number;
	this.fname = fname;
	this.lname = lname;
	this.password = password;
	
	this.endpoint = "api/instructor/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				fname: this.fname,
				lname: this.lname,
				password: this.password
			},
			url: this.endpoint + this.a_number,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				"property": property,
				"newValue": newValue
			},
			url: this.endpoint + this.a_number,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.a_number,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function Admin(username, fname, lname, password, access_level) {
	this.username = username;
	this.fname = fname;
	this.lname = lname;
	this.password = password;
	this.access_level = access_level;
	
	this.endpoint = "api/admin/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				fname: this.fname,
				lname: this.lname,
				password: this.password,
				access_level: this.access_level
			},
			url: this.endpoint + this.username,
 			success: [function(data) {console.log(data);}, successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.username,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.username,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function Class(class_id, a_number, class_name, late_time, school_id) {
	this.class_id = class_id;
	this.a_number = a_number;
	this.class_name = class_name;
	this.late_time = late_time;
	this.school_id = school_id;
	
	this.endpoint = "api/class/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				class_id: this.class_id,
				a_number: this.a_number,
				class_name: this.class_name,
				late_time: this.late_time,
				school_id: this.school_id
			},
			url: this.endpoint + this.class_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.class_id,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.class_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function ClassRegistration(registration_id, class_id, n_number, start_date, end_date) {
	this.registration_id = registration_id;
	this.class_id = class_id;
	this.n_number = n_number;
	this.start_date = start_date;
	this.end_date = end_date;
	
	this.endpoint = "api/class_registration/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				class_id: this.class_id,
				n_number: this.n_number,
				start_date: this.start_date,
				end_date: this.end_date
			},
			url: this.endpoint + this.registration_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.registration_id,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.registration_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function Klockin(registration_id, date, time_in, time_out, late) {
	this.registration_id = registration_id;
	this.date = date;
	this.time_in = time_in;
	this.time_out = time_out;
	this.late = late;
	
	this.endpoint = "api/klockin/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				registration_id: this.registration_id,
				date: this.date,
				time_in: this.time_in,
				time_out: this.time_out,
				late: this.late
			},
			url: this.endpoint,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				registration_id: this.registration_id,
				date: this.date,
				time_in: this.time_in,
				time_out: this.time_out,
				late: this.late,
				property: property,
				newValue: newValue
			},
			url: this.endpoint,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			data: {
				registration_id: this.registration_id,
				date: this.date,
				time_in: this.time_in,
				time_out: this.time_out,
				late: this.late
			},
			url: this.endpoint,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function School(school_id, name) {
	this.school_id = school_id;
	this.name = name;
	
	this.endpoint = "api/school/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				name: this.name
			},
			url: this.endpoint + this.school_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.school_id,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.school_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function Period(period_id, period_name, school_id, start_time, end_time) {
	this.period_id = period_id;
	this.period_name = period_name;
	this.school_id = school_id;
	this.start_time = start_time;
	this.end_time = end_time;
	
	this.endpoint = "api/period/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				period_name: this.period_name,
				school_id: this.school_id,
				start_time: this.start_time,
				end_time: this.end_time
			},
			url: this.endpoint + this.period_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				property: property,
				newValue: newValue
			},
			url: this.endpoint + this.period_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			url: this.endpoint + this.period_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
function PeriodRegistration(class_id, period_id) {
	this.class_id = class_id;
	this.period_id = period_id;
	
	this.endpoint = "api/period_registration/";
	
	this.insert = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_insert,
			dataType: dataType_json,
			data: {
				class_id: this.class_id,
				period_id: this.period_id
			},
			url: this.endpoint,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
	this.change = function(property, newValue, successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_change,
			dataType: dataType_json,
			data: {
				class_id: this.class_id,
				period_id: this.period_id,
				property: property,
				newValue: newValue
			},
			url: this.endpoint,
			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
		this[property] = newValue;
	};
	this.delete = function(successCallback, errorCallback, completeCallback) {
		$.ajax({
			type: httpReqType_delete,
			dataType: dataType_json,
			data: {
				class_id: this.class_id,
				period_id: this.period_id
			},
			url: this.endpoint + this.period_id,
 			success: [successCallback],
 			error: [errorCallback],
 			complete: [completeCallback]
		});
	};
}
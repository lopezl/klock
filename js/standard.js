var global_timeout;
function show_alert(message, classes, timeout) {
	timeout = timeout || 10000;
	clearTimeout(global_timeout);
	$("#alertContainer").html('<div id="alert" class="alert fade ' + classes + '" style="max-width: 700px;z-index: 1000; position: fixed; top: 10px; right: 0;left: 0;margin-right: auto;margin-left: auto; width: 50%;">\
	   <a href="#" class="close" onclick="hide_alert();">\
	      &times;\
	   </a>\
	   ' + message + '\
	</div>');
	$("#alert").addClass("in");
	if (timeout) {
		global_timeout = setTimeout(function () {
		    hide_alert();
		}, timeout);
	}
}

function hide_alert() {
	// fade out to then remove
	$("#alert").removeClass("in");
	clearTimeout(global_timeout);
	global_timeout = setTimeout(function () {
	 	$("#alert").remove();
	}, 500);
}

function getLoaderHtml(size, style) {
	size = size || "10px";
	style = style || "";
	return "<div class='loader' style='font-size:" + size + ";" + style + "'>Loading...</div>";
}

function show_loader_animation(selector, replace, size) {
	replace = replace || true;
	size = size || null;
	var loaderhtml = getLoaderHtml(size);
    if (replace) {
        $(selector).html(loaderhtml);
    } else {
        $(selector).append(loaderhtml);
    }
}

function hide_loader_animation() {
    $(".loader").remove();
}

function loadTable(htmlID, data, styleclasses) {
	prepareTable(htmlID);
	if (data.length > 1) {
		var tableHead = $("#" + htmlID + "_head");
		var tableBody = $("#" + htmlID + "_body");
		var tableFoot = $("#" + htmlID + "_foot");
		
		var headerRow = "<tr style='font-weight: bold;'>";
		for (var i = 0; i < data[0].length; i++) {
		    headerRow += "<td>" + data[0][i] + "</td>";
		}
		headerRow += "</tr>";
		tableHead.html(headerRow);
		
		tableBody.html("");
		for (var i = 1; i < data.length; i++) {
			var classes = "";
			if (typeof(styleclasses) !== 'undefined') {
				classes = styleclasses[i - 1].join(" ");
			}
		    var row = "<tr class=\'" + classes + "\'>";
			for (var j = 0; j < data[i].length; j++) {
				row += "<td>" + data[i][j] + "</td>";
			}
			row += "</tr>";
			tableBody.append(row);
		}
	} else {
		$("#" + htmlID).html("No records found");
	}
}

function prepareTable(htmlID) {
	$("#" + htmlID).html("<thead id=\"" + htmlID + "_head\">\
			</thead>\
			<tbody id=\"" + htmlID + "_body\">\
			</tbody>\
			<tfoot id=\"" + htmlID + "_foot\">\
			</tfoot>");
}

function prettifyName_r(name) {
	if (name.constructor === Array) {
		for (var i = 0; i < name.length; i++) {
			name[i] = prettifyName_r(name[i]);
		}
	} else {
		switch (name) {
		case "username":
			name = "Username";
			break;
		case "fname":
			name = "First Name";
			break;
		case "lname":
			name = "Last Name";
			break;
		case "password":
			name = "Password";
			break;
		case "access_level":
			name = "Access Level";
			break;
		case "class_id":
			name = "Class ID";
			break;
		case "a_number":
			name = "A Number";
			break;
		case "class_name":
			name = "Class Name";
			break;
		case "late_time":
			name = "Late Time";
			break;
		case "school_id":
			name = "School ID";
			break;
		case "registration_id":
			name = "Registration ID";
			break;
		case "n_number":
			name = "N Number";
			break;
		case "start_date":
			name = "Start Date";
			break;
		case "end_date":
			name = "End Date";
			break;
		case "date":
			name = "Date";
			break;
		case "time_in":
			name = "Time In";
			break;
		case "time_out":
			name = "Time Out";
			break;
		case "late":
			name = "Late";
			break;
		case "period_id":
			name = "Period ID";
			break;
		case "period_name":
			name = "Period Name";
			break;
		case "name":
			name = "Name";
			break;
		case "rfid":
			name = "RFID #";
			break;
		case "type":
			name = "Student Type";
			break;
		case "start_time":
			name = "Start Time";
			break;
		case "end_time":
			name = "End Time";
			break;
		}
	}
	
	return name;
}

function convertAPIResponseDataToTableFormat(responseData) {
	var keys = [];
	for (var i = 0; i < responseData.length; i++) {
		for (var key in responseData[i]) {
			var keyHasMatch = false;
			for (var j = 0; j < keys.length; j++) {
				if (key == keys[j]) {
					keyHasMatch = true;
				}
			}
			if (!keyHasMatch) {
				keys.push(key);
			}
		}
	}
	
	var dataTable = [];
	for (var i = 0; i < responseData.length; i++) {
		dataTable[i] = [];
		for (var j = 0; j < keys.length; j++) {
			var counter = 0;
			var wasMatched = false;
			for (var key in responseData[i]) {
				counter++;
					if (!wasMatched) {
					if (key == keys[j]) {
   						dataTable[i][j] = responseData[i][key];
						wasMatched = true;
   					} else if (counter == (Object.keys(responseData[i])).length - 1) {
   						dataTable[i][j] = null;
    				}
				}
				}
			}
	}
	
	keys = prettifyName_r(keys);
	dataTable.splice(0, 0, keys);
	return dataTable;
}
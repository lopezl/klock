<?php	
include 'includes/standard_strings.php';
include 'includes/loader.php';
include 'includes/database_class_structure.php';
include 'includes/database_connector.php';

session_start();

if (isset($_SESSION['instructor']) || isset($_SESSION['admin'])) {
	$content = '<br />
	<ul class="nav nav-tabs" id="primaryTabs">
		<li><a data-toggle="tab" href="#section_students">Students</a></li>
		<li><a data-toggle="tab" href="#section_instructors">Instructors</a></li>
		<li><a data-toggle="tab" href="#section_admins">Admins</a></li>
		<li><a data-toggle="tab" href="#section_periods">Periods</a></li>
		<li><a data-toggle="tab" href="#section_classes">Classes</a></li>
		<li><a data-toggle="tab" href="#section_schools">Schools</a></li>
		<li><a data-toggle="tab" href="#section_klockins">Klockins</a></li>
		<li><a data-toggle="tab" href="#section_class_registrations">Class Registrations</a></li>
		<li><a data-toggle="tab" href="#section_period_registrations">Period Registrations</a></li>
		<!--<li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a data-toggle="tab" href="#dropdown1">Dropdown1</a></li>
				<li><a data-toggle="tab" href="#dropdown2">Dropdown2</a></li>
			</ul>
		</li>-->
	</ul>
	<div class="tab-content">
		<br />
		<div id="section_blank" class="tab-pane fade in active">
		</div>
		
		
		
		<!-- Students -->
		
		<div id="section_students" class="tab-pane fade in">
			<button onclick="addNewStudent();" class="btn btn-primary">Add a Student</button>
			<form onsubmit="searchStudents($(\'#searchbox_students\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_students" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_students); searchTrigger_students(event);" autofocus></input>
					<div class="input-group-btn" style="margin: auto">
						<button class="btn btn-default" style="" id="submit_searchStudents" type="submit"><span style="margin: 7.5px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getStudents();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_students" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="studentsTable">
					
				</table>
			</div>
			
			
		</div>
		
		<!-- End Students -->
		<!-- Instructors -->
		
		<div id="section_instructors" class="tab-pane fade in">
			<button onclick="addNewInstructor();" class="btn btn-primary">Add an Instructor</button>
			<form onsubmit="searchInstructors($(\'#searchbox_instructors\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_instructors" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_instructors); searchTrigger_instructors(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchInstructors" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getInstructors();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_instructors" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="instructorsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Instructors -->
		<!-- Admins -->
		
		<div id="section_admins" class="tab-pane fade in">
			<button onclick="addNewAdmin();" class="btn btn-primary">Add an Admin</button>
			<form onsubmit="searchAdmins($(\'#searchbox_admins\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_admins" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_admins); searchTrigger_admins(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchAdmins" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getAdmins();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_admins" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="adminsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Admins -->
		<!-- Periods -->
		
		<div id="section_periods" class="tab-pane fade in">
			<button onclick="addNewPeriod();" class="btn btn-primary">Add a Period</button>
			<form onsubmit="searchPeriods($(\'#searchbox_periods\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_periods" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_periods); searchTrigger_periods(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchPeriods" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getPeriods();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_periods" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="periodsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Periods -->
		<!-- Classes -->
		
		<div id="section_classes" class="tab-pane fade in">
			<button onclick="addNewClass();" class="btn btn-primary">Add a Class</button>
			<form onsubmit="searchClasses($(\'#searchbox_classes\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_classes" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_classes); searchTrigger_classes(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchClasses" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getClasses();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_classes" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="classesTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Classes -->
		<!-- Schools -->
		
		<div id="section_schools" class="tab-pane fade in">
			<button onclick="addNewSchool();" class="btn btn-primary">Add a School</button>
			<form onsubmit="searchSchools($(\'#searchbox_schools\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_schools" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_classes); searchTrigger_schools(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchSchools" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getSchools();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_schools" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="schoolsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Schools -->
		<!-- Klockins -->
		
		<div id="section_klockins" class="tab-pane fade in">
			<button onclick="addNewKlockin();" class="btn btn-primary">Add a Klockin</button>
			<form onsubmit="searchKlockins($(\'#searchbox_klockins\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_schools" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_classes); searchTrigger_klockins(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchKlockins" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getKlockins();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_klockins" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="klockinsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Klockins -->
		<!-- Class Registrations -->
		
		<div id="section_class_registrations" class="tab-pane fade in">
			<button onclick="addNewClassRegistration();" class="btn btn-primary">Add a Class Registration</button>
			<form onsubmit="searchClassRegistrations($(\'#searchbox_class_registrations\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_class_registrations" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_classes); searchTrigger_klockins(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchClassRegistrations" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getClassRegistrations();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_class_registrations" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="class_registrationsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Class Registrations -->
		<!-- Period Registrations -->
		
		<div id="section_period_registrations" class="tab-pane fade in">
			<button onclick="addNewPeriodRegistration();" class="btn btn-primary">Add a Period Registration</button>
			<form onsubmit="searchPeriodRegistrations($(\'#searchbox_class_registrations\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
				<div class="input-group">
					<input class="form-control topInput bottomInput" id="searchbox_period_registrations" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_classes); searchTrigger_klockins(event);" autofocus></input>
					<div class="input-group-btn">
						<button class="btn btn-default" id="submit_searchPeriodRegistrations" type="submit"><span style="margin: 8px;" class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getPeriodRegistrations();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
			<div style="float:right;margin-top:10px;margin-right:30px;">Rows found: <span id="rowsFound_period_registrations" style="font-weight: bold;"></span></div>
			<div class="table-responsive" style="margin-top: 20px;">
				<table class="table" id="period_registrationsTable">
					
				</table>
			</div>
		</div>
		
		<!-- End Period Registrations -->
		
		<!--<div class="input-group date" id="datetimepicker4">
			<input type="text" class="form-control" />
			<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
			</span>
		</div>
		
		<script type="text/javascript">
			$(function () {
				$("#datetimepicker4").datetimepicker({
					pickDate: false
				});
			});
		</script>
		
		<div class="input-group date" id="datetimepicker1">
					<input type="text" class="form-control" />
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
				
				<script type="text/javascript">
			$(function () {
				$("#datetimepicker1").datetimepicker({
					format: "MM/DD/YYYY",
					pickTime: false
				});
			});
		</script>-->
		<!-- End Periods -->
	</div>';
	$headerScripts = '<script type="text/javascript src="js/manage.js"></script>
<script type="text/javascript">
	// Validates each keypress made by the user
	function fieldValidator(event, element) {
	if (event) {
		var result;
		var charCode;
		var max = element.getAttribute("max");
		if (event.which) {
			charCode = event.which;
		} else {
			charCode = event.keyCode;
		}
	
		// If the character entered is a number and would not exceed the max, let it through
		if (((charCode > 47 && charCode < 58) ||
			(charCode > 95 && charCode < 106)) &&
			element.value.length < max) {
			result = true;
	
		// If the above is not true, and the key pressed is left or right arrow key or the
		// enter key and their is nothing in textbox, then do not let it through
		} else if ((charCode == 37 || charCode == 39 || charCode == 13) && element.value.length > 0) {
			result = true;
	
		// If the above are not true, and the key pressed is either the backspace or delete key,
		// and their are characters in the textbox, let it through
		} else if ((charCode == 46 || charCode == 8) &&
			(element.value.length > 0)) {
			result = true;
	
		// If the above are not true, and the key pressed is f5, let it through
		} else if (charCode == 116) {
			result = true;
	// added tab support
	} else if (charCode == 9) {
	  result = true;
		// If none of the above are true, do not let through
		} else {
			result = false;
		}
		return result;
	}
}
	</script>';
	$footerScripts = '
	<script type="text/javascript">
	
	function setRowsFound(section, value) {
		$("#rowsFound_" + section).html(value);
	}
	
	$(document).ready(function(){
		$("#primaryTabs li a[href=\'#section_students\']").click();
		
		loadAllData();
	});
	
	function loadAllData() {
		getStudents();
		getInstructors();
		getAdmins();
		getPeriods();
		getClasses();
		getSchools();
		getKlockins();
		getClassRegistrations();
		getPeriodRegistrations();
	}
	
	
	var students_g = [];
	
	function getStudents(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#studentsTable");
		setRowsFound("students", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/student",
				data: {
					
				},
				dataType: "json",
				success: [function(students) {
					setRowsFound("students", students.data.length);
					var studentsTableData = convertAPIResponseDataToTableFormat(students.data);
					
					for (var i = 0; i < students.data.length; i++) {
						students_g[i] = new Student(
							students.data[i].fname,
							students.data[i].lname,
							students.data[i].n_number,
							students.data[i].rfid,
							students.data[i].type);
					}
					
					studentsTableData[0].unshift("Action");
					for (var i = 1; i < studentsTableData.length; i++) {
						studentsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteStudent(students_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editStudent(students_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("studentsTable", studentsTableData);
					
					var i = 0;
					$("#studentsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deleteStudent(student) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the student number: <b>" + student.n_number + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						student.delete(function() {getStudents();});
					}
				}
			}
		});
	}
	
	var searchTimeout_students;
	function searchTrigger_students(event) {
		if(searchTimeout_students) {
			clearTimeout(searchTimeout_students);
		}
		if ($("#searchbox_students").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_students_students = setTimeout(
					function() {
						if ($("#searchbox_students").val().length != 0) {
							$("#submit_searchStudents").click();
						}
					}, 250);
			}
		} else {
			getStudents();
		}
	}
	
	function changeStudent(student, fname, lname, n_number, rfid, type) {
		$.when(student.change("fname", fname),
				student.change("lname", lname),
				student.change("n_number", n_number),
				student.change("rfid", rfid),
				student.change("type", type)).done(function() {
			loadAllData();
			show_alert("Student changed successfully.", "alert-success");
		});
	}
	
	var studentToChange;
	function editStudent(student) {
		studentToChange = student;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changeStudent(studentToChange, $('."\'".'#student_fname'."\'".').val(), $('."\'".'#student_lname'."\'".').val(), $('."\'".'#student_n_number'."\'".').val(), $('."\'".'#student_rfid'."\'".').val(), $('."\'".'#student_type'."\'".').val()); bootbox.hideAll(); return false;">\
					<input value="\' + student.fname + \'" class="form-control topInput" id="student_fname" placeholder="First name" autofocus required/>\
					<input value="\' + student.lname + \'" class="form-control" id="student_lname" placeholder="Last name" required />\
					<input value="\' + student.n_number + \'" class="form-control" id="student_n_number" onkeydown="return fieldValidator(event, this);" placeholder="Student Number" max="6" required />\
					<input value="\' + student.rfid + \'" type="text" class="form-control" id="student_rfid" placeholder="RFID number (optional)" />\
					<select class="form-control bottomInput" id="student_type" placeholder="Type">\
						<option style="" value="Highschool student">Highschool student</option>\
						<option style="" value="Adult student">Adult student</option>\
					</select>\
					<button id="submit_editStudent" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Student",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change Student",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editStudent").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#student_type option").each(function(index, value) {
			if (student.type == $("#student_type option:nth-child(" + (index + 1) + ")").val()) {
				$("#student_type option:nth-child(" + (index + 1) + ")").prop("selected", true);
			}
		}
			
		);
	}
	
	function addNewStudent() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new Student($('."\'".'#student_fname'."\'".').val(), $('."\'".'#student_lname'."\'".').val(), $(student_n_number).val(), $('."\'".'#student_rfid'."\'".').val(), $('."\'".'#student_type'."\'".').val())).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="student_fname" placeholder="First name" autofocus required/>\
					<input class="form-control" id="student_lname" placeholder="Last name" required />\
					<input class="form-control" id="student_n_number" onkeydown="return fieldValidator(event, this);" placeholder="Student Number" max="6" required />\
					<input type="text" class="form-control" id="student_rfid" placeholder="RFID number (optional)" />\
					<select class="form-control bottomInput" id="student_type" placeholder="Type">\
						<option style="" value="Highschool student">Highschool student</option>\
						<option style="" value="Adult student">Adult student</option>\
					</select>\
					<button id="submit_AddNewStudent" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Student",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Student",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewStudent").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		
	}
	
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Instructors
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	
	var instructors_g = [];
	
	function getInstructors(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#instructorsTable");
		setRowsFound("instructors", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		//setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/instructor",
				data: {
					
				},
				dataType: "json",
				success: [function(instructors) {
					setRowsFound("instructors", instructors.data.length);
					var instructorsTableData = convertAPIResponseDataToTableFormat(instructors.data);
					
					for (var i = 0; i < instructors.data.length; i++) {
						instructors_g[i] = new Instructor(
							instructors.data[i].a_number,
							instructors.data[i].fname,
							instructors.data[i].lname,
							instructors.data[i].rfid);
					}
					
					instructorsTableData[0].unshift("Action");
					for (var i = 1; i < instructorsTableData.length; i++) {
						instructorsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteInstructor(instructors_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editInstructor(instructors_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("instructorsTable", instructorsTableData);
					
					var i = 0;
					$("#instructorsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
		//}, 1500);
	}
	
	function deleteInstructor(instructor) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the instructor number: <b>" + instructor.a_number + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						instructor.delete(loadAllData());
					}
				}
			}
		});
	}
	
	
	
	function searchInstructors(text) {
		if (text.length != 0) {
			getInstructors();
		}
	}
	
	var searchTimeout_instructors;
	function searchTrigger_instructors(event) {
		if(searchTimeout_instructors) {
			clearTimeout(searchTimeout_instructors);
		}
		if ($("#searchbox_instructors").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_instructors = setTimeout(
					function() {
						if ($("#searchbox_instructors").val().length != 0) {
							$("#submit_searchInstructors").click();
						}
					}, 250);
			}
		} else {
			getInstructors();
		}
	}
	
	function changeInstructor(instructor, fname, lname, a_number, password) {	
		$.when(instructor.change("fname", fname),
				instructor.change("lname", lname),
				instructor.change("a_number", a_number),
				instructor.change("password", password)).done(function() {
			loadAllData();
			show_alert("Instructor changed successfully.", "alert-success");
		});
	}
	
	var instructorToChange;
	function editInstructor(instructor) {
		instructorToChange = instructor;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="if($('."\'".'#instructor_password'."\'".').val() == $('."\'".'#instructor_password_confirm'."\'".').val()) { changeInstructor(instructorToChange, $('."\'".'#instructor_fname'."\'".').val(), $('."\'".'#instructor_lname'."\'".').val(), $('."\'".'#instructor_a_number'."\'".').val(), $('."\'".'#instructor_password'."\'".').val()); } else { show_alert('."\'".'Passwords entered do not match.'."\'".', '."\'".'alert-danger'."\'".'); } bootbox.hideAll(); return false;">\
					<input value="\' + instructor.fname + \'" class="form-control topInput" id="instructor_fname" placeholder="First name" autofocus required/>\
					<input value="\' + instructor.lname + \'" class="form-control" id="instructor_lname" placeholder="Last name" required />\
					<input value="\' + instructor.a_number + \'" class="form-control" id="instructor_a_number" onkeydown="return fieldValidator(event, this);" placeholder="Instructor Number" max="6" required />\
					<input value="" type="password" class="form-control" id="instructor_password" placeholder="New Password" />\
					<input value="" type="password" class="form-control bottomInput" id="instructor_password_confirm" placeholder="Confirm New Password" />\
					<button id="submit_editInstructor" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change an Instructor",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertInstructor_submit": {
					label: "Change Instructor",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editInstructor").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
	}
	
	function addNewInstructor() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="if ($('."\'".'#instructor_password'."\'".').val() == $('."\'".'#instructor_password_confirm'."\'".').val()) { (new Instructor($('."\'".'#instructor_a_number'."\'".').val(), $('."\'".'#instructor_fname'."\'".').val(), $('."\'".'#instructor_lname'."\'".').val(), $('."\'".'#instructor_password'."\'".').val())).insert(loadAllData()); } else { show_alert('."\'".'Passwords entered do not match.'."\'".', '."\'".'alert-danger'."\'".'); } bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="instructor_fname" placeholder="First name" autofocus required/>\
					<input class="form-control" id="instructor_lname" placeholder="Last name" required />\
					<input class="form-control" id="instructor_a_number" onkeydown="return fieldValidator(event, this);" placeholder="Instructor Number" max="6" required />\
					<input type="password" class="form-control" id="instructor_password" placeholder="New Password" />\
					<input type="password" class="form-control bottomInput" id="instructor_password_confirm" placeholder="Confirm New Password" />\
					<button id="submit_AddNewInstructor" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add an Instructor",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Instructor",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewInstructor").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
	}
	
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Admins
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	
	var admins_g = [];
	
	function getAdmins(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#adminsTable");
		setRowsFound("admins", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		//setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/admin",
				data: {
					
				},
				dataType: "json",
				success: [function(admins) {
					setRowsFound("admins", admins.data.length);
					var adminsTableData = convertAPIResponseDataToTableFormat(admins.data);
					
					for (var i = 0; i < admins.data.length; i++) {
						admins_g[i] = new Admin(
							admins.data[i].username,
							admins.data[i].fname,
							admins.data[i].lname,
							admins.data[i].password,
							admins.data[i].access_level);
					}
					
					adminsTableData[0].unshift("Action");
					for (var i = 1; i < adminsTableData.length; i++) {
						adminsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteAdmin(admins_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editAdmin(admins_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("adminsTable", adminsTableData);
					
					var i = 0;
					$("#adminsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
		//}, 1500);
	}
	
	function deleteAdmin(admin) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the admin username: <b>" + admin.username + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						admin.delete(loadAllData());
					}
				}
			}
		});
	}
	
	function searchAdmins(text) {
		if (text.length != 0) {
			getAdmins();
		}
	}
	
	var searchTimeout_admins;
	function searchTrigger_admins(event) {
		if(searchTimeout_admins) {
			clearTimeout(searchTimeout_admins);
		}
		if ($("#searchbox_admins").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_admins = setTimeout(
					function() {
						if ($("#searchbox_admins").val().length != 0) {
							$("#submit_searchAdmins").click();
						}
					}, 250);
			}
		} else {
			getAdmins();
		}
	}
	
	function changeAdmin(admin, username, fname, lname, password, access_level) {
		$.when(admin.change("username", username),
				admin.change("fname", fname),
				admin.change("lname", lname),
				admin.change("password", password),
				admin.change("access_level", access_level)).done(function() {
			loadAllData();
			show_alert("Admin changed successfully.", "alert-success");
		});
	}
	
	var adminToChange;
	function editAdmin(admin) {
		adminToChange = admin;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="if ($('."\'".'#admin_password'."\'".').val() == $('."\'".'#admin_password_confirm'."\'".').val()) { changeAdmin(adminToChange, $('."\'".'#admin_username'."\'".').val(), $('."\'".'#admin_fname'."\'".').val(), $('."\'".'#admin_lname'."\'".').val(), $('."\'".'#admin_password'."\'".').val(), $('."\'".'#admin_access_level'."\'".').val()) } else { show_alert('."\'".'Passwords entered do not match.'."\'".', '."\'".'alert-danger'."\'".'); }; bootbox.hideAll(); return false;">\
					<input value="\' + admin.fname + \'" class="form-control topInput" id="admin_fname" placeholder="First name" autofocus required/>\
					<input value="\' + admin.lname + \'" class="form-control" id="admin_lname" placeholder="Last name" required />\
					<input value="\' + admin.username + \'" class="form-control" id="admin_username" required />\
					<select class="form-control" id="admin_access_level" placeholder="Type">\
						<option value="Default admin">Default admin</option>\
						<option value="Uber admin">Uber admin</option>\
					</select>\
					<input value="" type="password" class="form-control" id="admin_password" placeholder="New Password" />\
					<input value="" type="password" class="form-control bottomInput" id="admin_password_confirm" placeholder="Confirm New Password" />\
					<button id="submit_editAdmin" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change an Admin",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertAdmin_submit": {
					label: "Change Admin",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editAdmin").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#admin_access_level option").each(function(index, value) {
			if (admin.access_level == $("#admin_access_level option:nth-child(" + (index + 1) + ")").val()) {
				$("#admin_access_level option:nth-child(" + (index + 1) + ")").prop("selected", true);
			}
		});
	}
	
	function addNewAdmin() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="if ($('."\'".'#admin_password'."\'".').val() == $('."\'".'#admin_password_confirm'."\'".').val()) { (new Admin($('."\'".'#admin_username'."\'".').val(), $('."\'".'#admin_fname'."\'".').val(), $('."\'".'#admin_lname'."\'".').val(), $('."\'".'#admin_password'."\'".').val(), $('."\'".'#admin_access_level'."\'".').val())).insert(loadAllData()); } else { show_alert('."\'".'Passwords entered do not match.'."\'".', '."\'".'alert-danger'."\'".'); } bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="admin_fname" placeholder="First name" autofocus required/>\
					<input class="form-control" id="admin_lname" placeholder="Last name" required />\
					<input class="form-control" id="admin_username" placeholder="Username" required />\
					<select class="form-control" id="admin_access_level" placeholder="Type">\
						<option value="Default admin">Default admin</option>\
						<option value="Uber admin">Uber admin</option>\
					</select>\
					<input type="password" class="form-control" id="admin_password" placeholder="New Password" />\
					<input type="password" class="form-control bottomInput" id="admin_password_confirm" placeholder="Confirm New Password" />\
					<button id="submit_AddNewAdmin" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add an Admin",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Admin",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewAdmin").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
	}
		
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Periods
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var periods_g = [];
	
	function getPeriods(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#periodsTable");
		setRowsFound("periods", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		//setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/period",
				data: {
					
				},
				dataType: "json",
				success: [function(periods) {
					setRowsFound("periods", periods.data.length);
					var periodsTableData = convertAPIResponseDataToTableFormat(periods.data);
					
					for (var i = 0; i < periods.data.length; i++) {
						periods_g[i] = new Period(
							periods.data[i].period_id,
							periods.data[i].period_name,
							periods.data[i].school_id,
							periods.data[i].start_time,
							periods.data[i].end_time);
					}
					
					periodsTableData[0].unshift("Action");
					for (var i = 1; i < periodsTableData.length; i++) {
						periodsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deletePeriod(periods_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editPeriod(periods_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("periodsTable", periodsTableData);
					
					var i = 0;
					$("#periodsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
		//}, 1500);
	}
	
	function deletePeriod(period) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the period number: <b>" + period.period_id + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						period.delete(loadAllData());
					}
				}
			}
		});
	}
	
	function searchPeriods(text) {
		if (text.length != 0) {
			getPeriods();
		}
	}
	
	var searchTimeout_periods;
	function searchTrigger_periods(event) {
		if(searchTimeout_periods) {
			clearTimeout(searchTimeout_periods);
		}
		if ($("#searchbox_periods").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_periods_periods = setTimeout(
					function() {
						if ($("#searchbox_periods").val().length != 0) {
							$("#submit_searchPeriods").click();
						}
					}, 250);
			}
		} else {
			getPeriods();
		}
	}
	
	function changePeriod(period, period_id, period_name, school_id, start_time, end_time) {
		$.when(period.change("period_id", period_id),
				period.change("period_name", period_name),
				period.change("school_id", school_id),
				period.change("start_time", start_time),
				period.change("end_time", end_time)).done(function() {
			loadAllData();
			show_alert("Period changed successfully.", "alert-success");
		});
	}
	
	var periodToChange;
	function editPeriod(period) {
		periodToChange = period;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changePeriod(periodToChange, $('."\'".'#period_period_id'."\'".').val(), $('."\'".'#period_period_name'."\'".').val(), $('."\'".'#period_school_id'."\'".').val(), $('."\'".'#period_start_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#period_end_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".')); bootbox.hideAll(); return false;">\
					<input value="\' + period.period_id + \'" class="form-control topInput" id="period_period_id" placeholder="Period ID" autofocus required/>\
					<input value="\' + period.period_name + \'" class="form-control" id="period_period_name" placeholder="Period Name" required />\
					<select class="form-control" id="period_school_id" placeholder="School ID" required>\
						\
					</select>\
					<div class="input-group date" id="period_start_time">\
			            <input type="text" class="form-control" oninput="" placeholder="Start Time" required/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="period_end_time">\
			            <input type="text" class="form-control" oninput="" placeholder="End Time" required/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<button id="submit_editPeriod" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Period",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertPeriod_submit": {
					label: "Change Period",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editPeriod").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#period_start_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#period_end_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#period_start_time").data("DateTimePicker").date(moment(period.start_time, "HH:mm:ss"));
		$("#period_end_time").data("DateTimePicker").date(moment(period.end_time, "HH:mm:ss"));
		$.ajax({
			type: "GET",
			url: "api/school",
			data: {
				
			},
			dataType: "json",
			success: function(schools) {
				for (var i = 0; i < schools.data.length; i++) {
					$("#period_school_id").append("<option value=\"" + schools.data[i].school_id + "\">" + schools.data[i].school_id + " - " + schools.data[i].name + "</option>");
				}
				$("#period_school_id option").each(function(index, value) {
					if (period.school_id == $("#period_school_id option:nth-child(" + (index + 1) + ")").val()) {
						$("#period_school_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
	}
	
	function addNewPeriod() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new Period($('."\'".'#period_period_id'."\'".').val(), $('."\'".'#period_period_name'."\'".').val(), $('."\'".'#period_school_id'."\'".').val(), $('."\'".'#period_start_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#period_end_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'))).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="period_period_id" placeholder="Period ID (optional)" autofocus/>\
					<input class="form-control" id="period_period_name" placeholder="Period Name" required />\
					<select class="form-control" id="period_school_id" placeholder="School ID" required>\
						\
					</select>\
					<div class="input-group date" id="period_start_time">\
			            <input type="text" class="form-control" oninput="" placeholder="Start Time" required/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="period_end_time">\
			            <input type="text" class="form-control" oninput="" placeholder="End Time" required/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<button id="submit_AddNewPeriod" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Period",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertPeriod_submit": {
					label: "Add Period",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewPeriod").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#period_start_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#period_end_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$.ajax({
			type: "GET",
			url: "api/school",
			data: {
				
			},
			dataType: "json",
			success: function(schools) {
				for (var i = 0; i < schools.data.length; i++) {
					$("#period_school_id").append("<option value=\"" + schools.data[i].school_id + "\">" + schools.data[i].school_id + " - " + schools.data[i].name + "</option>");
				}
			}
		});
	}
	
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Classes
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var classes_g = [];
	
	function getClasses(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#classesTable");
		setRowsFound("classes", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/class",
				data: {
					
				},
				dataType: "json",
				success: [function(classes) {
					setRowsFound("classes", classes.data.length);
					var classesTableData = convertAPIResponseDataToTableFormat(classes.data);
					
					for (var i = 0; i < classes.data.length; i++) {
						classes_g[i] = new Class(
							classes.data[i].class_id,
							classes.data[i].a_number,
							classes.data[i].class_name,
							classes.data[i].late_time,
							classes.data[i].school_id);
					}
					
					classesTableData[0].unshift("Action");
					for (var i = 1; i < classesTableData.length; i++) {
						classesTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteClass(classes_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editClass(classes_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("classesTable", classesTableData);
					
					var i = 0;
					$("#classesTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deleteClass(class_object) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the class ID: <b>" + class_object.class_id + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						class_object.delete(loadAllData());
					}
				}
			}
		});
	}
	
	var searchTimeout_classes;
	function searchTrigger_classes(event) {
		if(searchTimeout_classes) {
			clearTimeout(searchTimeout_classes);
		}
		if ($("#searchbox_classes").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_classes_classes = setTimeout(
					function() {
						if ($("#searchbox_classes").val().length != 0) {
							$("#submit_searchClasses").click();
						}
					}, 250);
			}
		} else {
			getClasses();
		}
	}
	
	function changeClass(class_object, class_id, a_number, class_name, late_time, school_id) {
		
		$.when(class_object.change("class_id", class_id),
				class_object.change("a_number", a_number),
				class_object.change("class_name", class_name),
				class_object.change("late_time", late_time),
				class_object.change("school_id", school_id)).done(function() {
			loadAllData();
			show_alert("Class changed successfully.", "alert-success");
		});
	}
	
	var classToChange;
	function editClass(class_object) {
		classToChange = class_object;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changeClass(classToChange, $('."\'".'#class_class_id'."\'".').val(), $('."\'".'#class_a_number'."\'".').val(), $('."\'".'#class_class_name'."\'".').val(), $('."\'".'#class_late_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#class_school_id'."\'".').val()); bootbox.hideAll(); return false;">\
					<input value="\' + class_object.class_id + \'" class="form-control topInput" id="class_class_id" placeholder="Class ID" autofocus required/>\
					<select class="form-control" id="class_a_number" placeholder="A Number" required>\
						\
					</select>\
					<input value="\' + class_object.class_name + \'" type="text" class="form-control" id="class_class_name" placeholder="Class Name" />\
					<div class="input-group date" id="class_late_time">\
			            <input type="text" class="form-control" oninput="" placeholder="Late Time" required/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<select class="form-control" id="class_school_id" placeholder="School ID" required>\
						\
					</select>\
					<button id="submit_editClass" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Class",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change Class",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editClass").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#class_type option").each(function(index, value) {
			if (class_object.type == $("#class_type option:nth-child(" + (index + 1) + ")").val()) {
				$("#class_type option:nth-child(" + (index + 1) + ")").prop("selected", true);
			}
		}
		);
		
		$("#class_late_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#class_late_time").data("DateTimePicker").date(moment(class_object.late_time, "HH:mm:ss"));
		$.ajax({
			type: "GET",
			url: "api/school",
			data: {
				
			},
			dataType: "json",
			success: function(schools) {
				for (var i = 0; i < schools.data.length; i++) {
					$("#class_school_id").append("<option value=\"" + schools.data[i].school_id + "\">" + schools.data[i].school_id + "</option>");
				}
				$("#class_school_id option").each(function(index, value) {
					if (class_object.school_id == $("#class_school_id option:nth-child(" + (index + 1) + ")").val()) {
						$("#class_school_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
		$.ajax({
			type: "GET",
			url: "api/instructor",
			data: {
				
			},
			dataType: "json",
			success: function(instructors) {
				for (var i = 0; i < instructors.data.length; i++) {
					$("#class_a_number").append("<option value=\"" + instructors.data[i].a_number + "\">" + instructors.data[i].a_number + " - " + instructors.data[i].fname + " " + instructors.data[i].lname + "</option>");
				}
				$("#class_a_number option").each(function(index, value) {
					if (class_object.a_number == $("#class_a_number option:nth-child(" + (index + 1) + ")").val()) {
						$("#class_a_number option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
	}
	
	function addNewClass() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="var late_time; if($('."\'".'#class_late_time input'."\'".').val() == '."\'".''."\'".') {late_time = null;} else {late_time = $('."\'".'#class_late_time'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".')} (new Class($('."\'".'#class_class_id'."\'".').val(), $('."\'".'#class_a_number'."\'".').val(), $('."\'".'#class_class_name'."\'".').val(), late_time, $('."\'".'#class_school_id'."\'".').val())).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="class_class_id" placeholder="Class ID (optional)" autofocus/>\
					<select class="form-control" id="class_a_number" placeholder="A Number" required>\
						\
					</select>\
					<input class="form-control" id="class_class_name" placeholder="Class Name" required />\
					<div class="input-group date" id="class_late_time">\
			            <input type="text" class="form-control" oninput="" placeholder="Late Time (optional)"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<select class="form-control" id="class_school_id" placeholder="School ID" required>\
						\
					</select>\
					<button id="submit_AddNewStudent" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Class",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Class",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewStudent").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#class_late_time").datetimepicker({
        	format: "HH:mm:ss"
        });
		$.ajax({
			type: "GET",
			url: "api/school",
			data: {
				
			},
			dataType: "json",
			success: function(schools) {
				for (var i = 0; i < schools.data.length; i++) {
					$("#class_school_id").append("<option value=\"" + schools.data[i].school_id + "\">" + schools.data[i].school_id + " - " + schools.data[i].name + "</option>");
				}
			}
		});
		$.ajax({
			type: "GET",
			url: "api/instructor",
			data: {
				
			},
			dataType: "json",
			success: function(instructors) {
				for (var i = 0; i < instructors.data.length; i++) {
					$("#class_a_number").append("<option value=\"" + instructors.data[i].a_number + "\">" + instructors.data[i].a_number + " - " + instructors.data[i].fname + " " + instructors.data[i].lname + "</option>");
				}
			}
		});
	}
	
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Schools
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var schools_g = [];
	
	function getSchools(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#schoolsTable");
		setRowsFound("schools", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/school",
				data: {
					
				},
				dataType: "json",
				success: [function(schools) {
					setRowsFound("schools", schools.data.length);
					var schoolsTableData = convertAPIResponseDataToTableFormat(schools.data);
					
					for (var i = 0; i < schools.data.length; i++) {
						schools_g[i] = new School(
							schools.data[i].school_id,
							schools.data[i].name);
					}
					
					schoolsTableData[0].unshift("Action");
					for (var i = 1; i < schoolsTableData.length; i++) {
						schoolsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteSchool(schools_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editSchool(schools_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("schoolsTable", schoolsTableData);
					
					var i = 0;
					$("#schoolsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deleteSchool(school) {
		bootbox.dialog({
			message: "Are you sure? This will remove all records dependant on the school ID: <b>" + school.school_id + "</b>",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						school.delete(loadAllData());
					}
				}
			}
		});
	}
	
	var searchTimeout_schools;
	function searchTrigger_schools(event) {
		if(searchTimeout_schools) {
			clearTimeout(searchTimeout_schools);
		}
		if ($("#searchbox_schools").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_schools_schools = setTimeout(
					function() {
						if ($("#searchbox_schools").val().length != 0) {
							$("#submit_searchClasses").click();
						}
					}, 250);
			}
		} else {
			getClasses();
		}
	}
	
	function changeSchool(school, school_id, name) {
		$.when(school.change("school_id", school_id),
				school.change("name", name)).done(function() {
			loadAllData();
			show_alert("School changed successfully.", "alert-success");
		});
	}
	
	var schoolToChange;
	function editSchool(school) {
		schoolToChange = school;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changeSchool(schoolToChange, $('."\'".'#school_school_id'."\'".').val(), $('."\'".'#school_name'."\'".').val()); bootbox.hideAll(); return false;">\
					<input value="\' + school.school_id + \'" class="form-control topInput" id="school_school_id" placeholder="School ID" autofocus required/>\
					<input value="\' + school.name + \'" class="form-control bottomInput" id="school_name" placeholder="School Name" required/>\
					<button id="submit_editSchool" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a School",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change School",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editSchool").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
	}
	
	function addNewSchool() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new School($('."\'".'#school_school_id'."\'".').val(), $('."\'".'#school_name'."\'".').val())).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="school_school_id" placeholder="School ID" autofocus required/>\
					<input class="form-control bottomInput" id="school_name" placeholder="School Name" required/>\
					<button id="submit_AddNewSchool" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a School",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add School",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewSchool").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
	}
	
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Klockins
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var klockins_g = [];
	
	function getKlockins(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#klockinsTable");
		setRowsFound("klockins", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/klockin",
				data: {
					
				},
				dataType: "json",
				success: [function(klockins) {
					setRowsFound("klockins", klockins.data.length);
					var klockinsTableData = convertAPIResponseDataToTableFormat(klockins.data);
					
					for (var i = 0; i < klockins.data.length; i++) {
						klockins_g[i] = new Klockin(
							klockins.data[i].registration_id,
							klockins.data[i].date,
							klockins.data[i].time_in,
							klockins.data[i].time_out,
							klockins.data[i].late);
					}
					
					klockinsTableData[0].unshift("Action");
					for (var i = 1; i < klockinsTableData.length; i++) {
						klockinsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteKlockin(klockins_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editKlockin(klockins_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("klockinsTable", klockinsTableData);
					
					var i = 0;
					$("#klockinsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deleteKlockin(klockin) {
		bootbox.dialog({
			message: "Are you sure?",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						klockin.delete(loadAllData());
					}
				}
			}
		});
	}
	
	var searchTimeout_klockins;
	function searchTrigger_klockins(event) {
		if(searchTimeout_schools) {
			clearTimeout(searchTimeout_schools);
		}
		if ($("#searchbox_schools").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_schools_schools = setTimeout(
					function() {
						if ($("#searchbox_schools").val().length != 0) {
							$("#submit_searchClasses").click();
						}
					}, 250);
			}
		} else {
			getClasses();
		}
	}
	
	function changeKlockin(klockin, registration_id, date, time_in, time_out, late) {
		$.when(klockin.change("registration_id", registration_id),
				klockin.change("date", date),
				klockin.change("time_in", time_in),
				klockin.change("time_out", time_out),
				klockin.change("late", late)).done(function() {
			loadAllData();
			show_alert("Klockin changed successfully.", "alert-success");
		});
	}
	
	var klockinToChange;
	function editKlockin(klockin) {
		klockinToChange = klockin;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changeKlockin(klockinToChange, $('."\'".'#klockin_registration_id'."\'".').val(), $('."\'".'#klockin_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".'), $('."\'".'#klockin_time_in'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#klockin_time_out'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#klockin_late'."\'".').val()); bootbox.hideAll(); return false;">\
					<select class="form-control" id="klockin_registration_id" placeholder="Registration ID" required>\
						\
					</select>\
					<div class="input-group date" id="klockin_date">\
			            <input type="text" class="form-control" oninput="" placeholder="Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="klockin_time_in">\
			            <input type="text" class="form-control" oninput="" placeholder="Time In"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="klockin_time_out">\
			            <input type="text" class="form-control" oninput="" placeholder="Time Out"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<input value="\' + klockin.late + \'" class="form-control bottomInput" id="klockin_late" placeholder="Late" required/>\
					<button id="submit_editKlockin" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Klockin",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change Klockin",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editKlockin").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#klockin_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$("#klockin_date").data("DateTimePicker").date(moment(klockin.date, "YYYY-MM-DD"));
		$("#klockin_time_in").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#klockin_time_in").data("DateTimePicker").date(moment(klockin.time_in, "HH:mm:ss"));
		$("#klockin_time_out").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#klockin_time_out").data("DateTimePicker").date(moment(klockin.time_out, "HH:mm:ss"));
		$.ajax({
			type: "GET",
			url: "api/class_registration",
			data: {
				
			},
			dataType: "json",
			success: function(class_registrations) {
				for (var i = 0; i < class_registrations.data.length; i++) {
					$("#klockin_registration_id").append("<option value=\"" + class_registrations.data[i].registration_id + "\">" + class_registrations.data[i].registration_id + "</option>");
				}
				$("#klockin_registration_id option").each(function(index, value) {
					if (klockin.registration_id == $("#klockin_class_registration option:nth-child(" + (index + 1) + ")").val()) {
						$("#klockin_registration_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
	}
	
	function addNewKlockin() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new Klockin($('."\'".'#klockin_registration_id'."\'".').val(), $('."\'".'#klockin_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".'), $('."\'".'#klockin_time_in'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#klockin_time_out'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'HH:mm:ss'."\'".'), $('."\'".'#klockin_late'."\'".').val())).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<select class="form-control" id="klockin_registration_id" placeholder="Registration ID" required>\
						\
					</select>\
					<div class="input-group date" id="klockin_date">\
			            <input type="text" class="form-control" oninput="" placeholder="Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="klockin_time_in">\
			            <input type="text" class="form-control" oninput="" placeholder="Time In"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date" id="klockin_time_out">\
			            <input type="text" class="form-control" oninput="" placeholder="Time Out"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<input class="form-control bottomInput" id="klockin_late" placeholder="Late" required/>\
					<button id="submit_AddNewKlockin" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Klockin",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Klockin",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_AddNewKlockin").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#klockin_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$("#klockin_date").data("DateTimePicker").date(moment());
		$("#klockin_time_in").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#klockin_time_in").data("DateTimePicker").date(moment());
		$("#klockin_time_out").datetimepicker({
        	format: "HH:mm:ss"
        });
		$("#klockin_time_out").data("DateTimePicker").date(moment());
		$.ajax({
			type: "GET",
			url: "api/class_registration",
			data: {
				
			},
			dataType: "json",
			success: function(class_registrations) {
				for (var i = 0; i < class_registrations.data.length; i++) {
					$("#klockin_registration_id").append("<option value=\"" + class_registrations.data[i].registration_id + "\">" + class_registrations.data[i].registration_id + "</option>");
				}
			}
		});
	}
	
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Class Registrations
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var class_registrations_g = [];
	
	function getClassRegistrations(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#class_registrationsTable");
		setRowsFound("class_registrations", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/class_registration",
				data: {
					
				},
				dataType: "json",
				success: [function(class_registrations) {
					setRowsFound("class_registrations", class_registrations.data.length);
					var class_registrationsTableData = convertAPIResponseDataToTableFormat(class_registrations.data);
					
					for (var i = 0; i < class_registrations.data.length; i++) {
						class_registrations_g[i] = new ClassRegistration(
							class_registrations.data[i].registration_id,
							class_registrations.data[i].class_id,
							class_registrations.data[i].n_number,
							class_registrations.data[i].start_date,
							class_registrations.data[i].end_date);
					}
					
					class_registrationsTableData[0].unshift("Action");
					for (var i = 1; i < class_registrationsTableData.length; i++) {
						class_registrationsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deleteClassRegistration(class_registrations_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editClassRegistration(class_registrations_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("class_registrationsTable", class_registrationsTableData);
					
					var i = 0;
					$("#klockinsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deleteClassRegistration(class_registration) {
		bootbox.dialog({
			message: "Are you sure?",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						class_registration.delete(loadAllData());
					}
				}
			}
		});
	}
	
	var searchTimeout_klockins;
	function searchTrigger_klockins(event) {
		if(searchTimeout_schools) {
			clearTimeout(searchTimeout_schools);
		}
		if ($("#searchbox_schools").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_schools_schools = setTimeout(
					function() {
						if ($("#searchbox_schools").val().length != 0) {
							$("#submit_searchClasses").click();
						}
					}, 250);
			}
		} else {
			getClasses();
		}
	}
	
	function changeClassRegistration(class_registration, registration_id, class_id, n_number, start_date, end_date) {
		$.when(class_registration.change("registration_id", registration_id),
				class_registration.change("class_id", class_id),
				class_registration.change("n_number", n_number),
				class_registration.change("start_date", start_date),
				class_registration.change("end_date", end_date)).done(function() {
			loadAllData();
			show_alert("Class registration changed successfully.", "alert-success");
		});
	}
	
	var class_registrationToChange;
	function editClassRegistration(class_registration) {
		class_registrationToChange = class_registration;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changeClassRegistration(class_registrationToChange, $('."\'".'#class_registration_registration_id'."\'".').val(), $('."\'".'#class_registration_class_id'."\'".').val(), $('."\'".'#class_registration_n_number'."\'".').val(), $('."\'".'#class_registration_start_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".'), $('."\'".'#class_registration_end_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".')); bootbox.hideAll(); return false;">\
					<input value="\' + class_registration.registration_id + \'" class="form-control topInput" id="class_registration_registration_id" placeholder="Registration ID" autofocus required/>\
					<select class="form-control" id="class_registration_class_id" placeholder="Class ID" required>\
						\
					</select>\
					<select class="form-control" id="class_registration_n_number" placeholder="N Number" required>\
						\
					</select>\
					<div class="input-group date" id="class_registration_start_date">\
			            <input type="text" class="form-control" oninput="" placeholder="Start Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date bottomInput" id="class_registration_end_date">\
			            <input type="text" class="form-control" oninput="" placeholder="End Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<button id="submit_editClassRegistration" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Class Registration",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change Class Registration",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editClassRegistration").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#class_registration_start_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$("#class_registration_start_date").data("DateTimePicker").date(moment(class_registration.start_date, "YYYY-MM-DD"));
		$("#class_registration_end_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$("#class_registration_end_date").data("DateTimePicker").date(moment(class_registration.end_date, "YYYY-MM-DD"));
		$.ajax({
			type: "GET",
			url: "api/class",
			data: {
				
			},
			dataType: "json",
			success: function(class_registrations) {
				for (var i = 0; i < class_registrations.data.length; i++) {
					$("#class_registration_class_id").append("<option value=\"" + class_registrations.data[i].class_id + "\">" + class_registrations.data[i].class_id + " - " + class_registrations.data[i].class_name + "</option>");
				}
				$("#class_registration_class_id option").each(function(index, value) {
					if (class_registration.class_id == $("#class_registration_class_id option:nth-child(" + (index + 1) + ")").val()) {
						$("#class_registration_class_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
		$.ajax({
			type: "GET",
			url: "api/student",
			data: {
				
			},
			dataType: "json",
			success: function(students) {
				for (var i = 0; i < students.data.length; i++) {
					$("#class_registration_n_number").append("<option value=\"" + students.data[i].n_number + "\">" + students.data[i].n_number + " - " + students.data[i].fname + " " + students.data[i].lname + "</option>");
				}
				$("#class_registration_n_number option").each(function(index, value) {
					if (class_registration.n_number == $("#class_registration_n_number option:nth-child(" + (index + 1) + ")").val()) {
						$("#class_registration_n_number option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
	}
	
	function addNewClassRegistration() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new ClassRegistration($('."\'".'#class_registration_registration_id'."\'".').val(), $('."\'".'#class_registration_class_id'."\'".').val(), $('."\'".'#class_registration_n_number'."\'".').val(), $('."\'".'#class_registration_start_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".'), $('."\'".'#class_registration_end_date'."\'".').data('."\'".'DateTimePicker'."\'".').date().format('."\'".'YYYY-MM-DD'."\'".'))).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<input class="form-control topInput" id="class_registration_registration_id" placeholder="Registration ID" autofocus/>\
					<select class="form-control" id="class_registration_class_id" placeholder="Class ID" required>\
						\
					</select>\
					<select class="form-control" id="class_registration_n_number" placeholder="N Number" required>\
						\
					</select>\
					<div class="input-group date" id="class_registration_start_date">\
			            <input type="text" class="form-control" oninput="" placeholder="Start Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<div class="input-group date bottomInput" id="class_registration_end_date">\
			            <input type="text" class="form-control" oninput="" placeholder="End Date"/>\
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>\
			        </div>\
					<button id="submit_addNewClassRegistration" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Class Registration",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Class Registration",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_addNewClassRegistration").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$("#class_registration_start_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$("#class_registration_end_date").datetimepicker({
        	format: "MM/DD/YYYY"
        });
		$.ajax({
			type: "GET",
			url: "api/class",
			data: {
				
			},
			dataType: "json",
			success: function(class_registrations) {
				for (var i = 0; i < class_registrations.data.length; i++) {
					$("#class_registration_class_id").append("<option value=\"" + class_registrations.data[i].class_id + "\">" + class_registrations.data[i].class_id + " - " + class_registrations.data[i].class_name + "</option>");
				}
			}
		});
		$.ajax({
			type: "GET",
			url: "api/student",
			data: {
				
			},
			dataType: "json",
			success: function(students) {
				for (var i = 0; i < students.data.length; i++) {
					$("#class_registration_n_number").append("<option value=\"" + students.data[i].n_number + "\">" + students.data[i].n_number + " - " + students.data[i].fname + " " + students.data[i].lname + "</option>");
				}
			}
		});
	}
	
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	// Period Registrations
	// ---------------------------------------------------------------------
	// ---------------------------------------------------------------------
	
	var period_registrations_g = [];
	
	function getPeriodRegistrations(successCallback, errorCallback, completeCallback) {
		show_loader_animation("#period_registrationsTable");
		setRowsFound("period_registrations", getLoaderHtml("2px", "display: inline-block; margin-left: 10px; bottom: 5px;"));
		setTimeout(function() {
			$.ajax({
				type: "GET",
				url: "api/period_registration",
				data: {
					
				},
				dataType: "json",
				success: [function(period_registrations) {
					setRowsFound("period_registrations", period_registrations.data.length);
					var period_registrationsTableData = convertAPIResponseDataToTableFormat(period_registrations.data);
					
					for (var i = 0; i < period_registrations.data.length; i++) {
						period_registrations_g[i] = new PeriodRegistration(
							period_registrations.data[i].class_id,
							period_registrations.data[i].period_id
						)
					}
					
					period_registrationsTableData[0].unshift("Action");
					for (var i = 1; i < period_registrationsTableData.length; i++) {
						period_registrationsTableData[i].unshift(\'<button type="button" class="btn btn-danger" aria-label="Delete" onclick="deletePeriodRegistration(period_registrations_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\
							</button>\
							<button type="button" class="btn btn-default" aria-label="Change" onclick="editPeriodRegistration(period_registrations_g[\' + (i - 1) + \'])">\
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
							</button>\');
					}
					loadTable("period_registrationsTable", period_registrationsTableData);
					
					var i = 0;
					$("#klockinsTable_body").children().each(function() {
						$(this).children(":first").attr("style", "width: 110px;");
						$(this).data("key", i);
						
						i++;
					});
				}, successCallback],
				error: errorCallback,
				complete: completeCallback
			});
			
		}, 1500);
	}
	
	function deletePeriodRegistration(period_registration) {
		bootbox.dialog({
			message: "Are you sure?",
			title: "Confirm Deletion",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default",
					callback: function() {
						
					}
				},
				confirm: {
					label: "Yes",
					className: "btn-danger",
					callback: function() {
						period_registration.delete(loadAllData());
					}
				}
			}
		});
	}
	
	var searchTimeout_klockins;
	function searchTrigger_klockins(event) {
		if(searchTimeout_schools) {
			clearTimeout(searchTimeout_schools);
		}
		if ($("#searchbox_schools").val().length != 0) {
			if (event.keyCode != 13) {
				searchTimeout_schools_schools = setTimeout(
					function() {
						if ($("#searchbox_schools").val().length != 0) {
							$("#submit_searchPeriodes").click();
						}
					}, 250);
			}
		} else {
			getPeriodes();
		}
	}
	
	function changePeriodRegistration(period_registration, class_id, period_id) {
		$.when(period_registration.change("class_id", class_id),
				period_registration.change("period_id", period_id)).done(function() {
			loadAllData();
			show_alert("Period registration changed successfully.", "alert-success");
		});
	}
	
	var period_registrationToChange;
	function editPeriodRegistration(period_registration) {
		period_registrationToChange = period_registration;
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="changePeriodRegistration(period_registrationToChange, $('."\'".'#period_registration_class_id'."\'".').val(), $('."\'".'#period_registration_period_id'."\'".').val()); bootbox.hideAll(); return false;">\
					<select class="form-control" id="period_registration_class_id" placeholder="Class ID" required>\
						\
					</select>\
					<select class="form-control" id="period_registration_period_id" placeholder="Period ID" required>\
						\
					</select>\
					<button id="submit_editPeriodRegistration" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Change a Period Registration",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Change Period Registration",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_editPeriodRegistration").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$.ajax({
			type: "GET",
			url: "api/class",
			data: {
				
			},
			dataType: "json",
			success: function(classes) {
				for (var i = 0; i < classes.data.length; i++) {
					$("#period_registration_class_id").append("<option value=\"" + classes.data[i].class_id + "\">" + classes.data[i].class_id + " - " + classes.data[i].class_name + "</option>");
				}
				$("#period_registration_class_id option").each(function(index, value) {
					if (period_registration.class_id == $("#period_registration_class_id option:nth-child(" + (index + 1) + ")").val()) {
						$("#period_registration_class_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
		$.ajax({
			type: "GET",
			url: "api/period",
			data: {
				
			},
			dataType: "json",
			success: function(periods) {
				for (var i = 0; i < periods.data.length; i++) {
					$("#period_registration_period_id").append("<option value=\"" + periods.data[i].period_id + "\">" + periods.data[i].period_id + " - " + periods.data[i].period_name + "</option>");
				}
				$("#period_registration_period_id option").each(function(index, value) {
					if (period_registration.period_id == $("#period_registration_period_id option:nth-child(" + (index + 1) + ")").val()) {
						$("#period_registration_period_id option:nth-child(" + (index + 1) + ")").prop("selected", true);
					}
				});
			}
		});
	}
	
	function addNewPeriodRegistration() {
		var box = bootbox.dialog({
			message: \'\
				<form class="form" role="form" onsubmit="(new PeriodRegistration($('."\'".'#period_registration_class_id'."\'".').val(), $('."\'".'#period_registration_period_id'."\'".').val())).insert(loadAllData()); bootbox.hideAll(); return false;">\
					<select class="form-control" id="period_registration_class_id" placeholder="Class ID" required>\
						\
					</select>\
					<select class="form-control" id="period_registration_period_id" placeholder="Period ID" required>\
						\
					</select>\
					<button id="submit_addNewPeriodRegistration" type="submit" style="display: none;"></button>\
				</form>\',
			title: "Add a Period Registration",
			buttons: {
				"Cancel": {
					label: "Cancel",
					className: "btn btn-default"
				},
				"btninsertStudent_submit": {
					label: "Add Period Registration",
					className: "btn btn-primary",
					callback: function() {
						$("#submit_addNewPeriodRegistration").click();
						return false;
					}
				}
			}
		});
		box.bind("shown.bs.modal", function(){
			box.find("input[autofocus]").focus();
		});
		$.ajax({
			type: "GET",
			url: "api/class",
			data: {
				
			},
			dataType: "json",
			success: function(classes) {
				for (var i = 0; i < classes.data.length; i++) {
					$("#period_registration_class_id").append("<option value=\"" + classes.data[i].class_id + "\">" + classes.data[i].class_id + " - " + classes.data[i].class_name + "</option>");
				}
			}
		});
		$.ajax({
			type: "GET",
			url: "api/period",
			data: {
				
			},
			dataType: "json",
			success: function(periods) {
				for (var i = 0; i < periods.data.length; i++) {
					$("#period_registration_period_id").append("<option value=\"" + periods.data[i].period_id + "\">" + periods.data[i].period_id + " - " + periods.data[i].period_name + "</option>");
				}
			}
		});
	}
	
</script>
	<script type="text/javascript">
		
	</script>';
	$title = 'Manage';
	
	$user = (isset($_SESSION['instructor']) ? $_SESSION['instructor'] : $_SESSION['admin']);
	$requires_temp_authentication = true;
	if ($user->temp_authenticated) {
		$user->temp_authenticated = false;
		(isset($_SESSION['instructor']) ? $_SESSION['instructor'] = $user : $_SESSION['admin'] = $user);
		$requires_temp_authentication = false;
	}
	
	echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$manage_menu_item_id, $title, $requires_temp_authentication);
} else{
	$content = '<center><h1>Forbidden</h1></center><p>You do not have the permissions to view this page</p>';
	$headerScripts = '';
	$footerScripts = '';
	$title = 'Manage';
	
	echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$manage_menu_item_id, $title);
}
?>

<?php
include '../includes/database_class_structure.php';
include '../includes/database_connector.php';
include '../includes/loader.php';
include '../includes/standard_strings.php';
require '../includes/password.php';

session_start();

error_reporting(E_ALL ^ E_NOTICE);

// Requests from the same server don't have a HTTP_ORIGIN header
// if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
//	 $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
// }

$api = new API($_REQUEST['request']/*, $_SERVER['HTTP_ORIGIN']*/);
echo $api->processAPI();

class API {
	// Property: method
	// The HTTP method this request was made in, either GET, POST, PUT or DELETE
	protected $method = '';
	
	// Property: endpoint
	// The Model requested in the URI. eg: /files
	protected $endpoint = '';
	
	// Property: verb
	// An optional additional descriptor about the endpoint, used for things that can
	// not be handled by the basic methods. eg: /files/process
	//protected $verb = '';
	
	// Property: args
	// Any additional URI components after the endpoint and verb have been removed, in our
	// case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
	// or /<endpoint>/<arg0>
	protected $args = Array();
	
	// Property: file
	// Stores the input of the PUT request
	protected $file = Null;
	
	// Property: connector
	// Stores the databases connector for endpoints to perform functions
	protected $connector;

	// Constructor: __construct
	// Allow for CORS, assemble and pre-process the data
	public function __construct($request) {
		// header('Access-Control-Allow-Orgin: *');
		// header('Access-Control-Allow-Methods: *');
		header('Content-Type: application/json');
		
		$this->connector = new database_connector();
		
		// Takes the string of arguments given, seperated by slashes, and parses that into an array
		// where the endpoint and verb is then shifted out leaving an array of arguments, and two
		// vars representing the endpoint and verb.
		$this->args = explode('/', rtrim($request, '/'));
		$this->endpoint = array_shift($this->args);
		
		// My personal touch, to make stuff make a little more sense when using the API
		if ($this->endpoint == 'class') {
			$this->endpoint = 'class_object';
		}
		/*if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
			$this->verb = array_shift($this->args);
		}*/
		foreach ($this->args as $key => &$value) {
			if ($value == 'null') {
				$value = null;
			}
			if ($value == 'true') {
				$value = true;
			} else if ($value == 'false') {
				$value = false;
			}
		}
		if (isset($this->request['data'])) {
			$this->request['data'] = json_decode($this->request['data']);
		}
		
		$this->method = $_SERVER['REQUEST_METHOD'];
		if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
			if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
				$this->method = 'DELETE';
			} else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
				$this->method = 'PUT';
			} else {
				throw new Exception('Unexpected Header');
			}
		}
		
		$this->file = file_get_contents('php://input');
		
		switch($this->method) {
			case 'POST':
				$this->request = $this->_cleanInputs($_POST);
				break;
			case 'GET':
				$this->request = $this->_cleanInputs($_GET);
				break;
			case 'DELETE':
				parse_str($this->file, $this->request);
				break;
			case 'PUT':
				parse_str($this->file, $this->request);
				break;
			default:
				throw new Exception("Invalid Method", 405);
				break;
		}
		
		$this->request = $this->_cleanInputs($this->request);
	}
	
	public function processAPI() {
		try {
			if (method_exists($this, $this->endpoint)) {
				//if (isset($this->verb)) {
				return $this->_response($this->{$this->endpoint}($this->args));
				//} else {
				//	throw new Exception('No verb specified');
				//}
			} else {
				throw new Exception('No Endpoint: ' . $this->endpoint, 404);
			}
		} catch (Exception $e) {
			return $this->_response(new response(false, $e->getMessage()));
		}
	}

	private function _response($data, $status = 200) {
		header('HTTP/1.1 ' . $status . ' ' . $this->_requestStatus($status));
		return $this->_formatData($data);
	}
	
	private function _formatData($data) {
		return json_encode($data, JSON_PRETTY_PRINT);
	}

	private function _cleanInputs($data) {
		$clean_input = Array();
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				$clean_input[$k] = $this->_cleanInputs($v);
			}
		} else {
			$clean_input = trim(strip_tags($data));
		}
		return $clean_input;
	}

	private function _requestStatus($code) {
		$status = array(  
			200 => 'OK',
			404 => 'Not Found',   
			405 => 'Method Not Allowed',
			500 => 'Internal Server Error'
		); 
		return ($status[$code])?$status[$code]:$status[500]; 
	}
	
	// Sample endpoint
	protected function sample() {
		if ($this->method == 'POST') {
				
			return new response(true);
				
		} else if ($this->method == 'DELETE') {
				
			return new response(true);
				
		} else if ($this->method == 'PUT') {
				
			return new response(true);
				
		} else if ($this->method == 'GET') {
				
			return new response(true);
				
		}
	}
	
	// -------------------------------------------------------------------------
	// API functions unique to Klock
	// -------------------------------------------------------------------------
	
	protected function authenticate() {
		if ($this->args[0] == 'logout') {
			if (isset($_SESSION['instructor'])) {
				unset($_SESSION['instructor']);
			}
			if (isset($_SESSION['admin'])) {
				unset($_SESSION['admin']);
			}
			return new response(true);
		}
		if ($this->args[0] == 'login') {
			$connector = new database_connector();
			$authenticated = false;
			$userType = null;
			$userObject = null;
			
			// Find the admin with the username provided if possible
			$admins = $connector->run_query('SELECT * FROM admins WHERE username=?', array($this->request['username']));
			
			// If there was an admin found with the given username, construct and admin object, verify the password,
			// and echo '2' to indicate that the user is an administrator
			if (!empty($admins)) {
				$object = $admins[0];
				$admin = admin::construct_with_array($admins[0]);
				if (password_verify($this->request['password'], $admin->getHash())) {
					$_SESSION['admin'] = $admin;
					$authenticated = true;
					$userType = 'admin';
					$userObject = $admin;
				}
				
				// If there were no admins found with the given username, check for instructor username
			} else {
				
				// Get the instructor with the username provided, use same verification method as above
				$instructors = $connector->run_query('SELECT * FROM instructors WHERE a_number=?', array($this->request['username']));
				if (!empty($instructors)) {
					$instructor = instructor::construct_with_array($instructors[0]);
					if (password_verify($this->request['password'], $instructor->getHash())) {
						$_SESSION['instructor'] = $instructor;
						$authenticated = true;
						$userType = 'instructor';
						$userObject = $instructor;
					}
					// If this test also fails, then the user has provided an invalid username or password or both
				}
			}
			
			return new response(true, null, array('authenticated' => $authenticated, 'userType' => $userType, 'userObject' => $userObject));
		}
		if ($this->args[0] == 'temp') {
			$connector = new database_connector();
			$authenticated = false;
			$userType = null;
			$userObject = null;
			
			// Find the admin with the username provided if possible
			if (isset($_SESSION['admin'])) {
				$admin = $connector->run_query('SELECT * FROM admins WHERE username=?', array($_SESSION['admin']->username));
				$admin = admin::construct_with_array($admin[0]);
				if (password_verify($this->request['password'], $admin->getHash())) {
					$_SESSION['admin']->temp_authenticated = true;
					$authenticated = true;
					$userType = '';
					$userObject = $_SESSION['admin'];
				}
			} else if (isset($_SESSION['instructor'])) {
				$instructor = $connector->run_query('SELECT * FROM instructors WHERE a_number=?', array($_SESSION['instructor']->a_number));
				$instructor = instructor::construct_with_array($instructor[0]);
				if (password_verify($this->request['password'], $instructor->getHash())) {
					$_SESSION['instructor']->temp_authenticated = true;
					$authenticated = true;
					$userType = '';
					$userObject = $_SESSION['instructor'];
				}
			}
			
			return new response(true, null, array('authenticated' => $authenticated, 'userType' => $userType, 'userObject' => $userObject));
		}
	}
	
	// Student endpoint
	protected function student() {
		if ($this->method == 'POST') {
			
			$student = new student($this->request['fname'], $this->request['lname'], $this->args[0], $this->request['rfid'], $this->request['type']);
			$student->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$student = student::construct_with_id($this->args[0]);
			$student->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$student = student::construct_with_id($this->args[0]);
			$student->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('students');
			
		}
	}
	
	// Instructor endpoint
	protected function instructor() {
		if ($this->method == 'POST') {
			
			$instructor = new instructor($this->args[0], $this->request['fname'], $this->request['lname'], $this->request['password'], true);
			$instructor->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$instructor = instructor::construct_with_id($this->args[0]);
			$instructor->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$instructor = instructor::construct_with_id($this->args[0]);
			$instructor->change($this->request['property'], $this->request['newValue']);
			return new response(true, $this->args[0], $this->request);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('instructors', array('password'));
			
		}
	}
	
	// Admin endpoint
	protected function admin() {
		if ($this->method == 'POST') {
			
			$admin = new admin($this->args[0], $this->request['fname'], $this->request['lname'], $this->request['password'], $this->request['access_level'], true);
			$admin->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$admin = admin::construct_with_id($this->args[0]);
			$admin->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$admin = admin::construct_with_id($this->args[0]);
			$admin->change($this->request['property'], $this->request['newValue']);
			return new response(true, $this->args[0]);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('admins', array('password'));
			
		}
	}
	
	// Class endpoint
	protected function class_object() {
		if ($this->method == 'POST') {
			
			$class = new class_object($this->args[0], $this->request['a_number'], $this->request['class_name'], $this->request['late_time'], $this->request['school_id']);
			$class->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$class = class_object::construct_with_id($this->args[0]);
			$class->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$class = class_object::construct_with_id($this->args[0]);
			$class->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('classes');
			
		}
	}
	
	protected function class_registration() {
		if ($this->method == 'POST') {
			
			$class_registration = new class_registration($this->args[0], $this->request['class_id'], $this->request['n_number'], $this->request['start_date'], $this->request['end_date']);
			$class_registration->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$class_registration = class_registration::construct_with_id($this->args[0]);
			$class_registration->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$class_registration = class_registration::construct_with_id($this->args[0]);
			$class_registration->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('class_registrations');
			
		}
	}
	
	protected function klockin() {
		if ($this->args[0] == 'submit') {
			// -----------------------------------------------------------------------------
			// Initializing crucial values. The testing mode values should be used with
			// testing, they will provide the same functions as the live values.
			// Choose which values you need by commenting or uncommenting different lines.
			// It is ok to choose different live values and testing values at the same time,
			// just as long as they are not the same variable
			// -------------------------------------
			
			// Live values:
			$utc_timezone = new DateTimeZone('UTC');
			$student_number = $this->args[1];
			$datetime_now = new DateTime('now', new DateTimeZone('America/New_York'));
			$a_number = $_SESSION['instructor']->a_number;
			$this->connector = new database_connector();
			
			// Test values:
			//$student_number = '145944';
			//$a_number = '053918';
			//													  Enter your timezone here
			//$datetime_now = (new DateTime('12:10:00', new DateTimeZone('UTC')))->setTimezone($utc_timezone);
			
			// -------------------------------------
			// End section.
			// -----------------------------------------------------------------------------
			
			// The buffer allows students to klock in x sec early
			$class_time_buffer = (5 * 60); // In seconds (5 minutes)
			
			// Adds the specified buffers to special DateTime objects
			$datetime_with_class_buffer = (new DateTime(date('H:i:s', $datetime_now->getTimestamp() + $class_time_buffer)))->setTimezone($utc_timezone);
			
			// Initializes return variables to default values
			$there_is_late = false;
			$return = '';
			$klocked_in = false;
			
			// -----------------------------------------------------------------------------
			// This area finds classes of interest
			// -------------------------------------
			// Loops, until one of the tests finds classes of interest, with a maximum of 2 loop-throughs.
			// After the second loop-through with no results, the script should move to the bottom thus denying a klockin
			$classes_of_interest = array();
			for ($i = 0; $i < 2 && sizeof($classes_of_interest) == 0; $i++) {
				
				// The argument array to send into the SQL statement
				$arg_array = array($student_number, $a_number, $datetime_now->format('H:i:s'), $datetime_now->format('H:i:s'));
				
				// Performs a more lax search on the second iteration by taking into account the buffer zone,
				// in-case the student is attempting to klock in early, thus causing the first test to fail
				if ($i == 1) {
					$arg_array[2] = $datetime_with_class_buffer->format('H:i:s');
				}
				
				// Starts looking for the instructor's classes that the student's n-number is associated with based on time
				$result = $this->connector->run_query('SELECT clas.*
					FROM class_registrations AS clas_reg
					JOIN classes AS clas, 
					period_registrations AS per_reg, 
					periods AS per
					WHERE clas_reg.n_number=?
					AND clas.a_number=?
					AND clas_reg.class_id=clas.class_id
					AND clas.class_id=per_reg.class_id
					AND per_reg.period_id=per.period_id
					AND per.start_time<=?
					AND per.end_time>=?
					ORDER BY per.start_time', $arg_array);
				$classes_of_interest = array_merge($classes_of_interest, $result);
			}
			for ($i = 0; $i < sizeof($classes_of_interest); $i++) {
				$classes_of_interest[$i] = class_object::construct_with_array($classes_of_interest[$i]);
			}
			for ($i = 0; $i < sizeof($classes_of_interest); $i++) {
				$class_registrations_of_interest = $this->connector->run_query('SELECT *
						FROM class_registrations
						WHERE class_id=?', array($classes_of_interest[$i]->class_id));
				for ($i = 0; $i < sizeof($class_registrations_of_interest); $i++) {
					$class_registrations_of_interest[$i] = class_registration::construct_with_array($class_registrations_of_interest[$i]);
				}
			}
			
			// Handles the return values if no classes of interest are found
			if (sizeof($classes_of_interest) == 0) {
				$result = $this->connector->run_query('SELECT * FROM instructors WHERE a_number=?', array($a_number));
				$instructor = instructor::construct_with_array($result[0]);
				$return = 'Not registered for a class with <b>' . $instructor->fname . ' ' . $instructor->lname . '</b> at this time';
			}
			// -------------------------------------
			// End section.
			// -----------------------------------------------------------------------------
			
			// -----------------------------------------------------------------------------
			// This section handles the logic behind late times primarily, but also deals
			// with whether to klock in or out.
			// This section will be bypassed if there are no classes of interest found.
			// -------------------------------------
			// Loops through all classes of interest
			for ($i = 0; $i < sizeof($classes_of_interest); $i++) {
				$class_of_interest = $classes_of_interest[$i];
				$class_registration_of_interest = $class_registrations_of_interest[$i];
				
				// Find klockins associated with classes of interest where the 'time_out' is null
				$klockins_with_null_klockouts = $this->connector->run_query('SELECT klockins.* FROM klockins
						WHERE klockins.registration_id=?
						AND klockins.time_out IS null', array($class_registration_of_interest->registration_id));
				for ($i = 0; $i < sizeof($klockins_with_null_klockouts); $i++) {
					$klockins_with_null_klockouts[$i] = klockin::construct_with_array($klockins_with_null_klockouts[$i]);
				}
				
				// If there were any klockins with null 'time_out'
				if (sizeof($klockins_with_null_klockouts) > 0) {
					// Update all klockin rows associated with the current class of interest, and set the time_out value to the current time
					$this->connector->run_query('UPDATE klockins SET time_out=? WHERE klockins.registration_id=?
							AND klockins.time_out IS null', array($datetime_now->format('H:i:s'), $class_registration_of_interest->registration_id));
					// Now we need to find the class associated with the class registration of the current class of interest
					// so we can get specific information about it (class name, etc.)
					$result = $this->connector->run_query('SELECT clas.* FROM class_registrations AS clas_reg
							JOIN classes AS clas
							WHERE clas_reg.registration_id=?
							AND clas_reg.class_id=clas.class_id', array ($class_registration_of_interest->registration_id));
					$class = class_object::construct_with_array($result[0]);
					
					// If there were no klockins with their 'time_out' value set to null for the current class of interest,
					// then we need to create and insert a new klockin row
				} else {
					// -----------------------------------------------------------------------------
					// This sub-section handles the logic behind what defines "late".
					// Touch and you die.
					// -------------------------------------
					// According to the class's start and late times, determine whether the student is late (ignoring breaks)
					// If not, simply move on with the assumption that the student is on time
					$late = false;
					if ($class_of_interest->late_time != null && $class_of_interest->start_time->getTimestamp() + $class_of_interest->late_time->getTimestamp() < $datetime_now->getTimestamp()) {
						$late = true;
					}
					$there_is_late = $late;
					/*if ($student_number == '145944' || $student_number == '163682') {
						$late = false;
					}*/
					// ..... hehe.. XD
					
					// -------------------------------------
					// End sub-section.
					// -----------------------------------------------------------------------------
					
					// Create the new klockin
					$klockin = new klockin($class_registration_of_interest->registration_id, $datetime_now->format('Y-m-d'), $datetime_now->format('H:i:s'), null, $late);
					
					// Insert the new klockin into the database
					$klockin->insert();
					$klocked_in = true;
				}	
			}
			
			$instructor = instructor::construct_with_id($a_number);
			return new response(true, null, array('late' => $there_is_late, 'instructor' => $instructor->fname . ' ' . $instructor->lname, 'class' => $class_of_interest->class_name, 'klocked_in' => $klocked_in));
			
		} else if ($this->method == 'POST') {
			
			$klockin = new klockin($this->request['registration_id'], $this->request['date'], $this->request['time_in'], $this->request['time_out'], $this->request['late']);
			$klockin->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$klockin = new klockin($this->request['registration_id'], $this->request['date'], $this->request['time_in'], $this->request['time_out'], $this->request['late']);
			$klockin->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$klockin = new klockin($this->request['registration_id'], $this->request['date'], $this->request['time_in'], $this->request['time_out'], $this->request['late']);
			$klockin->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('klockins');
			
		}
	}
	
	protected function school() {
		if ($this->method == 'POST') {
			
			$school = new school($this->args[0], $this->request['name']);
			$school->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$school = school::construct_with_id($this->args[0]);
			$school->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$school = school::construct_with_id($this->args[0]);
			$school->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('schools');
			
		}
	}
	
	protected function period() {
		if ($this->method == 'POST') {
			
			$period = new period($this->args[0], $this->request['period_name'], $this->request['school_id'], $this->request['start_time'], $this->request['end_time']);
			$period->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$period = period::construct_with_id($this->args[0]);
			$period->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$period = period::construct_with_id($this->args[0]);
			$period->change($this->request['property'], $this->request['newValue']);
			return new response(true);
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('periods');
			
		}
	}
	
	protected function period_registration() {
		if ($this->method == 'POST') {
			
			$period_registration = new period_registration($this->request['class_id'], $this->request['period_id']);
			$period_registration->insert();
			return new response(true);
			
		} else if ($this->method == 'DELETE') {
			
			$period_registration = new period_registration($this->request['class_id'], $this->request['period_id']);
			$period_registration->delete();
			return new response(true);
			
		} else if ($this->method == 'PUT') {
			
			$period_registration = new period_registration($this->request['class_id'], $this->request['period_id']);
			$period_registration->change($this->request['property'], $this->request['newValue']);
			return new response(true, "dfsdfsdfsd");
			
		} else if ($this->method == 'GET') {
			
			return $this->getRecordsUsingReqParams('period_registrations');
			
		}
	}
	
	protected function getRecordsUsingReqParams($tablename, $ignorecolumns = array()) {
		$columns = $this->connector->run_query("SHOW COLUMNS FROM " . $tablename, array(), PDO::FETCH_NUM);
		$columnNames = array();
		for ($j = 0; $j < sizeof($columns); $j++) {
			$columnNames[] = $columns[$j][0];
		}
		
		$keys = array_keys($this->request);
		$args = array();
		for ($i = 0; $i < sizeof($this->request); $i++) {
			if ($keys[$i] != 'request') {
				$args[$keys[$i]] = $this->request[$keys[$i]];
			}
		}
		
		/*for ($i = 0; $i < sizeof($columns); $i++) {
			for ($j = 0; $j < sizeof($ignorecolumns); $j++) {
				if ($columns[$i][0] == $ignorecolumns[$j]) {
					 unset($columns[$i]);
					 $columns = array_values($columns);
					 $i--;
				}
			}
		}*/
		
		//print_r(buildSqlQueryConditions($tablename, $args));
		
		/*for ($i = 0; $i < sizeof($args); $i++) {
			if ($args[$i][0] == 'search') {
				for($j = 0; $j < sizeof($columns); $j++) {
					$ignore = false;
					for ($k = 0; $k < sizeof($ignorecolumns); $k++) {
						if ($columns[$j][0] == $ignorecolumns[$k]) {
							$ignore = true;
						}
					}
					if (!$ignore) {
						$search_conditions['key'][] = $columns[$j][0] . " LIKE ?";
						$search_conditions['value'][] = '%' . $args[$i][1] . '%';
					}
				}
			} else {
				if (valueIsInArray($args[$i][0], $columnNames)) {
					if (is_array($args[$i][1])) {
						$keys = array();
						foreach ($args[$i][1] as $key => $val) {
							array_push($keys, $key);
						}
						if ($keys[0] == 'date' && valueIsInArray('dateLowerBound', $columnNames)) {
							$matchesColumn = false;
							for ($k = 0; $k < sizeof($columns); $k++) {
								if ($args[$i][0] == $columns[$k][0]) {
									//$matchesColumn = true;
								}
								if ($k == sizeof($columns) - 1 && matchesColumn) {
									
								}
							}
						} else {
							$conditions['key'][$i] = array(sizeof($args[$i][1]));
							for ($k = 0; $k < sizeof($args[$i][1]); $k++) {
								$conditions['key'][$i][$k] = $args[$i][0] . '=?';
							}
							$conditions['key'][$i] = implode(' OR ', $conditions['key'][$i]);
							$conditions['key'][$i] = '(' . $conditions['key'][$i] . ')';
							$conditions['value'][] = $args[$i][1];
							array_splice($conditions['value'], $i, 1, $args[$i][1]);
						}
					} else {
						$conditions['key'][$i] = $args[$i][0] . '=?';
						$conditions['value'][] = $args[$i][1];
						print_r($args);
						
					}
				}
			}
		}*/
		
		//$records = $this->connector->run_query("SELECT * FROM " . $tablename . (sizeof($conditions['key']) > 0 || sizeof($search_conditions['key']) > 0 ? " WHERE " . (isset($search_conditions) ? "(" . implode(' OR ', $search_conditions['key']) . ")" . (isset($conditions) ? " AND " : "") : "") . (isset($conditions) ? implode(' AND ', $conditions['key']) : "") : ""), array_merge((isset($search_conditions['value']) ? $search_conditions['value'] : array()), (isset($conditions['value']) ? $conditions['value'] : array())), PDO::FETCH_ASSOC);
		$conditions = buildSqlQueryConditions($tablename, $args, $ignorecolumns);
		$query = "SELECT * FROM " . $tablename;
		if ($conditions[0] !== '') {
			$query .= ' WHERE ' . $conditions[0];
		}
		//print_r($args);
		//echo $query;
		$records = $this->connector->run_query($query, $conditions[1], PDO::FETCH_ASSOC);
		return new response(true, null, $records);
	}
}

function buildSqlQueryConditions($tablename, $args, $ignoreColumns = array()) {
	$conditionString = "";
	$conditionArray = array();
	$conditionArgsArray = array();
	
	$columns = (new database_connector())->run_query("SHOW COLUMNS FROM " . $tablename, array(), PDO::FETCH_NUM);
	$columnNames = array();
	for ($j = 0; $j < sizeof($columns); $j++) {
		$columnNames[] = $columns[$j][0];
	}
	$keys = array_keys($args);
	if (isAssoc($args)) {
		for ($i = 0; $i < sizeof($args); $i++) {
			if(valueIsInArray($keys[$i], $columnNames)) {
				if (is_array($args[$keys[$i]])) {
					$secondaryKeys = array_keys($args[$keys[$i]]);
					if (valueIsInArray('greaterThanOrEqualTo', $secondaryKeys) || valueIsInArray('lessThanOrEqualTo', $secondaryKeys) || valueIsInArray('greaterThan', $secondaryKeys) || valueIsInArray('lessThan', $secondaryKeys)) {
						$subConditionArray = array();
						$subConditionArgsArray = array();
						for ($j = 0; $j < sizeof($args[$keys[$i]]); $j++) {
							if ($secondaryKeys[$j] == 'greaterThanOrEqualTo') {
								$subConditionArray[] = $keys[$i] . '>=?';
								$subConditionArgsArray[] = $args[$keys[$i]][$secondaryKeys[$j]];
							} else if ($secondaryKeys[$j] == 'lessThanOrEqualTo') {
								$subConditionArray[] = $keys[$i] . '<=?';
								$subConditionArgsArray[] = $args[$keys[$i]][$secondaryKeys[$j]];
							} else if ($secondaryKeys[$j] == 'greaterThan') {
								$subConditionArray[] = $keys[$i] . '>?';
								$subConditionArgsArray[] = $args[$keys[$i]][$secondaryKeys[$j]];
							} else if ($secondaryKeys[$j] == 'lessThan') {
								$subConditionArray[] = $keys[$i] . '<?';
								$subConditionArgsArray[] = $args[$keys[$i]][$secondaryKeys[$j]];
							}
						}
						if (sizeof($subConditionArray) > 0) {
							$conditionArray[] = '(' . implode(' AND ', $subConditionArray) . ')';
						}
						$conditionArgsArray = array_merge($conditionArgsArray, $subConditionArgsArray);
					} else {
						if (!isAssoc($args[$keys[$i]])) {
							array_unshift($args[$keys[$i]], $keys[$i]);
						}
						$result = buildSqlQueryConditions($tablename, $args[$keys[$i]], $ignoreColumns);
						$conditionArray[] = $result[0];
						$conditionArgsArray = array_merge($conditionArgsArray, $result[1]);
					}
				} else {
					$conditionArray[] = $keys[$i] . '=?';
					$conditionArgsArray[] = $args[$keys[$i]];
				}
			} else if ($keys[$i] == 'search') {
				if (is_array($args[$keys[$i]])) {
					array_unshift($args[$keys[$i]], $keys[$i]);
				} else {
					$args[$keys[$i]] = array($keys[$i], $args[$keys[$i]]);
				}
				$result = buildSqlQueryConditions($tablename, $args[$keys[$i]], $ignoreColumns);
				$conditionArray[] = $result[0];
				$conditionArgsArray = array_merge($conditionArgsArray, $result[1]);
			}
		}
		if (sizeof($conditionArray) > 0) {
			$conditionString .= '(' . implode(' AND ', $conditionArray) . ')';
		}
	} else {
		$columnName = array_shift($args);
		if ($columnName == 'search') {
			for ($i = 0; $i < sizeof($args); $i++) {
				$subConditionArray = array();
				$subConditionArgsArray = array();
				for ($j = 0; $j < sizeof($columnNames); $j++) {
					if (!valueIsInArray($columnNames[$j], $ignoreColumns)) {
						$subConditionArray[] = $columnNames[$j] . " LIKE ?";
						$subConditionArgsArray[] = '%' . $args[$keys[$i]] . '%';
					}
				}
				if (sizeof($subConditionArray) > 0) {
					$conditionArray[] = '(' . implode(' OR ', $subConditionArray) . ')';
				}
				$conditionArgsArray = array_merge($conditionArgsArray, $subConditionArgsArray);
			}
		} else if (valueIsInArray($columnName, $columnNames)) {
			for ($i = 0; $i < sizeof($args); $i++) {
				if (is_array($args[$i])) {
					if (isAssoc($args[$i])) {
						$result = buildSqlQueryConditions($tablename, array($columnName => $args[$i]), $ignoreColumns);
						$conditionArray[] = $result[0];
						$conditionArgsArray = array_merge($conditionArgsArray, $result[1]);
					} else {
						array_unshift($args[$i], $columnName);
						$result = buildSqlQueryConditions($tablename, $args[$i], $ignoreColumns);
						$conditionArray[] = $result[0];
						$conditionArgsArray = array_merge($conditionArgsArray, $result[1]);
					}
				} else {
					$conditionArray[] = $columnName . '=?';
					$conditionArgsArray[] = $args[$i];
				}
			}
		}
		if (sizeof($conditionArray)) {
			$conditionString .= '(' . implode(' OR ', $conditionArray) . ')';
		}
	}
	return array($conditionString, $conditionArgsArray);
}

function isAssoc($array) {
	return !($array === array_values($array));
}

function valueIsInArray($value, $array, $recursive = true) {
	$match = false;
	for ($i = 0; $i < sizeof($array); $i++) {
		if ($recursive && is_array($array[$i])) {
			if (valueIsInArray($value, $array[$i])) {
				$match = true;
			}
		} else {
			if ($value === $array[$i]) {
				$match = true;
			}
		}
	}

	return $match;
}

class response {
	public $success;
	public $text;
	public $data;
	
	public function __construct($success = false, $text = null, $data = null) {
		$this->success = $success;
		$this->text = $text;
		$this->data = $data;
	}
}

?>
<?php
include 'includes/standard_strings.php';
include 'includes/loader.php';

session_start();
if (isset($_SESSION['instructor']) || isset($_SESSION['admin'])) {
    if (isset($_SESSION['instructor'])) {
        $date = (new DateTime("now", new DateTimeZone('America/New_York')));
        $current_timestamp_in_local_timezone = $date->getTimestamp();
        //echo $date->format('Y-m-d H:i:s');
    
        $title = 'Student klock in';
        $content = '<!-- Klock-in -->
  <div>
       <form class="form" autocomplete="off" onsubmit="submitForm(); return false;">
         <h1 class="form-heading">' . standard_strings::$studentKlockin . '</h1>
         <div id="date_and_time_unformatted" style="display: none;">' . (($current_timestamp_in_local_timezone) * 1000) . '</div>
         <h1 id="date_and_time_formatted" style="text-align: center; font-size: 50px;"></h1>
         <input style="text-align: center;" onkeydown="return fieldValidator(event, this);" onkeyup="keyupSubmitTrigger();" oninput="hide_infobox(\'fast\');" name="' . standard_strings::$klockin_field_name . '" type="text" max="6" class="form-control topInput bottomInput" placeholder="' . standard_strings::$studentNumber . '" required autocomplete="off" pattern="\d{6}">
         <button class="btn btn-lg btn-primary btn-block" id="submitBtn" type="submit">Klock in</button>
         <div id="info_div_outer" class="info-box">
           <div id="info_div_inner"></div>
         </div>
       </form>
     </div>';
    
        $headerScripts = '<script type="text/javascript">
    var red = {
        red:"255", 
        green:"50", 
        blue:"0", 
        opacity:""
    }
    var green = {
        red:"0", 
        green:"153", 
        blue:"0", 
        opacity:""
    }
    var blue = {
        red:"0", 
        green:"0", 
        blue:"255", 
        opacity:""
    }
    var yellow = {
        red:"221", 
        green:"221", 
        blue:"0", 
        opacity:""
    }
    
    var info_colors = {
        red:{
            red:	"255", 
            green:	"50", 
            blue:	"0"
        }, 
        green:{
            red:	"0", 
            green:	"200", 
            blue:	"0"
        }, 
        blue:{
            red:	"0", 
            green:	"100", 
            blue:	"255"
        }, 
        yellow:{
            red:	"230", 
            green:	"230", 
            blue:	"0"
        }
    };
    
    var timeout;
    
    function hide_infobox(speed) {
        innerInfoDiv.fadeOut(speed, "swing");
        outerInfoDiv.fadeOut(speed, "swing");
    }
    
    function show_infobox(text, color) {
        if (text != null && text != "") {
            innerInfoDiv.html(text);
        }
        if (color != null) {
            outerInfoDiv.attr("style", "background-color: rgba(" + color.red + "," + color.green + "," + color.blue + ",0.3); border-color: rgba(" + color.red + "," + color.green + "," + color.blue + ",0.5);");
        }
    
        outerInfoDiv.fadeIn("slow", "swing");
        innerInfoDiv.fadeIn("slow", "swing");
    
        setTimeout(function () {
            hide_infobox("slow");
        }, (15 * 1000));
    }
    
    // Clears the value of the field and does the neat little animation if user has stopped typing for 3 seconds
    function keyupSubmitTrigger() {
        if(timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        if (studentNumber.attr("value").length != 0) {
            timeout = setTimeout(
                function() {
                    if (studentNumber.attr("value").length != 0) {
                        submitForm();
                    }
                }, 3000);
        }
    }
    
    // Handling the click procedure for the submit button
    function submitForm() {
        clearTimeout(timeout);
        timeout = null;
    
        if (studentNumber.attr("value").length == 6 && studentNumber.attr("value").match(/[a-z]/) == null) {
            $.ajax({
                type: "POST",
                url: "api/klockin/submit/" + studentNumber.attr("value"),
        		data: {"n_number" : studentNumber.attr("value")},
                success: function(data) {
        				if (data.success) {
        					if (data.data.class != null) {
        						if (data.data.klocked_in) {
        							if (data.data.late) {
        								animateBorder(info_colors.yellow);
                        				show_infobox("Klocked in to " + data.data.class, info_colors.yellow);
        							} else {
        								animateBorder(info_colors.green);
                        				show_infobox("Klocked in to " + data.data.class, info_colors.green);
        							}
        						} else {
        							animateBorder(info_colors.blue);
                        			show_infobox("Klocked out of " + data.data.class, info_colors.blue);
        						}
        					} else {
        						animateBorder(info_colors.red);
                        		show_infobox("You are not registered for a class with " + data.data.instructor + " at this time", info_colors.red);
        					}
        				} else {
        					animateBorder(info_colors.red);
                        	show_infobox("' . standard_strings::$error_horrible . '", info_colors.red);
        				}
                    },
                error: function(data) {
                        animateBorder(info_colors.red);
                        show_infobox("' . standard_strings::$error_horrible . '", info_colors.red);
                    },
            });
    
            if(timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
        } else {
            animateBorder(info_colors.red);
            show_infobox("' . standard_strings::$error_invalid . '", info_colors.red);
        }
    
        clearKlockinField();
    }
    
    function clearKlockinField() {
        studentNumber.attr("value", "");
    }
    
    // Neat little animation for the Student Klock-in field
    function animateBorder(color) {
        setTimeout(function() {
            var original_style = studentNumber.attr("style");
            studentNumber.css({"border-color" : "rgba(" + color.red + "," + color.green + "," + color.blue + ",1)"}).stop(true, false).animate(
                {"border-width":"10px"},
                "fast",
                "swing",
                setTimeout(function() {
                    studentNumber.animate(
                        {"border-width":"1px"},
                        "fast",
                        "swing",
                        function() {
                            studentNumber.attr("style", original_style);
                        }
                    );
                }, 0)
            )
        ;}, 0);
    }
    
// Validates each keypress made by the user
function fieldValidator(event, element) {
    if (event) {
        var result;
        var charCode;
        var max = element.getAttribute("max");
        if (event.which) {
            charCode = event.which;
        } else {
            charCode = event.keyCode;
        }
    
        // If the character entered is a number and would not exceed the max, let it through
        if (((charCode > 47 && charCode < 58) ||
            (charCode > 95 && charCode < 106)) &&
            element.value.length < max) {
            result = true;
    
        // If the above is not true, and the key pressed is left or right arrow key or the
        // enter key and their is nothing in textbox, then do not let it through
        } else if ((charCode == 37 || charCode == 39 || charCode == 13) && element.value.length > 0) {
            result = true;
    
        // If the above are not true, and the key pressed is either the backspace or delete key,
        // and their are characters in the textbox, let it through
        } else if ((charCode == 46 || charCode == 8) &&
            (element.value.length > 0)) {
            result = true;
    
        // If the above are not true, and the key pressed is f5, let it through
        } else if (charCode == 116) {
            result = true;
    
        // If none of the above are true, do not let through
        } else {
            result = false;
        }
        return result;
    }
}
 </script>';
    
        $footerScripts = '<script type="text/javascript">
    var studentNumber = $("[name=\'' . standard_strings::$klockin_field_name . '\']");
    var dateTimeFormatted = $("#date_and_time_formatted");
    var dateTimeUnformatted = $("#date_and_time_unformatted");
    
    var outerInfoDiv = $("#info_div_outer");
    var innerInfoDiv = $("#info_div_inner");
    
    outerInfoDiv.attr("style", "background-color: rgba(" + info_colors.red.red + "," + info_colors.red.green + "," + info_colors.red.blue + ",0.3); border-color: rgba(" + info_colors.red.red + "," + info_colors.red.green + "," + info_colors.red.blue + ",0.5);");
    
    outerInfoDiv.hide();
    innerInfoDiv.hide();
    
    increment_timeclock();
    
    function increment_timeclock() {
        var date_object = new Date(parseInt(dateTimeUnformatted.html()) + performance.now());
        var date_and_time = "";
    
        var year = date_object.getFullYear();
        var month = date_object.getMonth() + 1;
        var date = date_object.getDate();
    
        var hours = date_object.getHours();
        var minutes = date_object.getMinutes();
        var seconds = date_object.getSeconds();
        var ampm = "am";
    
        if (hours > 12) {
            ampm = "pm";
            hours -= 12;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
    
        date_and_time = year + "-" + month + "-" + date + "<br/><b>" + hours + ":" + minutes + ":" + seconds + " " + ampm + "</b>";
        dateTimeFormatted.html(date_and_time);
    }
    
    setInterval(function() {
        increment_timeclock()
    },
    100);
    
    setInterval(function () {
        studentNumber.focus();
    }, 500)
  </script>';
    
        echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$klockin_menu_item_id, $title);
    } else {
        header('Location:reports.php');
    }
} else {
    header('Location:index.php');
}
?>
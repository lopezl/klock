<?php
require_once('tcpdf/tcpdf.php');
include 'includes/database_connector.php';
include 'includes/database_class_structure.php';
$connector = new database_connector();

// extend TCPF with custom functions
class MYPDF extends TCPDF {
	// Colored table
	public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(22, 124, 196);
		$this->SetTextColor(255);
		$this->SetDrawColor(11, 100, 160);
		$this->SetLineWidth(0);
		$this->SetFont('', 'B');
		// Header
		$w = array(30, 45, 45, 30, 30);
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(224, 235, 255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) {
			$this->Cell($w[0], 6, $row[0], 'RB', 0, 'L', $fill);
			$this->Cell($w[1], 6, $row[1], 'RB', 0, 'L', $fill);
			$this->Cell($w[2], 6, $row[2], 'RB', 0, 'L', $fill);
			$this->Cell($w[3], 6, $row[3], 'RB', 0, 'L', $fill);
			$this->Cell($w[4], 6, $row[4], 'B', 0, 'L', $fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', '0');
	}
}
if(!isset($_GET['a_number']) || !isset($_GET['start_date']) || !isset($_GET['end_date'])){
	echo 'ERROR: no GET data provided';
}
else{
	$a_number = $_GET['a_number'];
	$start_date = $_GET['start_date'];
	$end_date = $_GET['end_date'];
	if(!isset($_GET['save'])){
		$save = false;
	}
	else{
		$save = $_GET['save'];
	}
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	$pdf->SetCreator('Klock');
	$pdf->SetAuthor('Klock');
	$pdf->SetTitle('Klock Report');
	$pdf->SetSubject('Attendance report');
	$pdf->SetKeywords('klock, report, attendance');

	$pdf->SetHeaderData('../images/klock-pdf.png', 15, 'Klock', 'Attendance report', array(0,0,0),array(0,0,0));
	//$pdf->setFooterData(array(0,64,0), array(0,64,128));

	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	if (@file_exists(dirname(__FILE__).'/tcpdf/lang/eng.php')) {
		require_once(dirname(__FILE__).'/tcpdf/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	$pdf->setFontSubsetting(true);

	// column titles
	$header = array('Date', 'First name', 'Last name', 'Time in', 'Time out');
	
	$class_array = $connector->run_query("SELECT cls.class_id, cls.class_name, per.start_time, per.end_time FROM classes JOIN period_registrations AS pr, classes AS cls, periods AS per WHERE cls.a_number = ? AND cls.class_id = pr.class_id AND pr.period_id = per.period_id", array($a_number), PDO::FETCH_NUM);
	for($i = 0; $i < sizeof($class_array); $i++){
		$pdf->AddPage();
		// get class info
		$class_id = $class_array[$i][0];
		$class_name = $class_array[$i][1];
		$start_time = $class_array[$i][2];
		$end_time = $class_array[$i][3];
		// write class info at top of page
		// if dates are the same then don't put it as range
		if($start_date == $end_date){
			$pdf->Write(1,'Date: ' . $start_date, '', false, '', true);
		}
		else{
			$pdf->Write(1,'Date: ' . $start_date . ' to ' . $end_date, '', false, '', true);
		}
		$pdf->Write(1,'Name: ' . $class_name, '', false, '', true);
		$pdf->Write(1,'Time: ' . $start_time . ' to ' . $end_time, '', false, '', true);
		$class_table = $connector->run_query("SELECT klockins.date, std.fname, std.lname, klockins.time_in, klockins.time_out FROM klockins JOIN class_registrations AS cr, classes AS cl, students AS std WHERE cl.class_id = ? AND cl.class_id = cr.class_id AND klockins.registration_id = cr.registration_id AND (klockins.date BETWEEN ? AND ?) AND cr.n_number = std.n_number ORDER BY klockins.date ASC", array($class_id, $start_date, $end_date), PDO::FETCH_NUM);
		$pdf->ColoredTable($header, $class_table);
	}
	
	// This outputs the PDF to the browser screen, use D to force download
	// the filename is the a#_report_ISODATE.pdf
	if($save){
		$mode = "D";
	}
	else{
		$mode = "I";
	}
	$pdf->Output('a' . $a_number .'_report_' . date('c') . '.pdf', $mode);
}
?>

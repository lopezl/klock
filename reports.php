<?php
include 'includes/standard_strings.php';
include 'includes/loader.php';
include 'includes/database_connector.php';
include 'includes/database_class_structure.php';

session_start();
$connector = new database_connector();

if (isset($_SESSION['instructor']) || isset($_SESSION['admin'])) {
    $content = '
    <div style="text-align: center;"><h1>Reports</h1></div>
		<ul class="nav nav-tabs" id="primaryTabs">
	        <li><a data-toggle="tab" href="#section_attendance">Attendance</a></li>
	        <li><a data-toggle="tab" href="#section_time">Time</a></li>
	        
	    </ul>
    	
    	<div class="tab-content">
    		<br />
    		<div id="section_blank" class="tab-pane fade in active">
        	</div>
    		
	    	<div id="section_attendance" class="tab-pane fade in">
	            <div class="form" style="text-align:left; margin: 0;">
			    	<select class="form-control" id="attendance_instructor" placeholder="Type" onchange="populateAvailableClasses($(\'#attendance_instructor\').find(\':selected\').data(\'value\').a_number);">
				    	
				    </select>
			    	<select class="form-control" id="attendance_class" placeholder="Type">
				    	
				    </select>
			    	<div class="input-group date" id="attendanceDateTimePicker">
			            <input type="text" class="form-control topInput bottomInput" oninput=""/>
			            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
			            </span>
			        </div>
			    </div>
    			<button onclick="loadAttendance();" class="btn btn-primary">Load Attendance</button>
    			<button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="loadAttendance();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
    			<div class="table-responsive">
			        <table class="table" id="reportsTable">
			        	
			        </table>
		        </div>
	        </div>
	    	
	    	<div id="section_time" class="tab-pane fade in">
	            <button onclick="addNewStudent();" class="btn btn-primary">Add a Student</button>
	            <form onsubmit="searchStudents($(\'#searchbox_students\').val()); return false;" class="form" style="padding: 0px; position: relative; float: right;">
	                <div class="input-group">
	                    <input class="form-control topInput bottomInput" id="searchbox_students" placeholder="Search..." onkeypress="" oninput="clearTimeout(window.searchTimeout_students); searchTrigger_students(event);" autofocus></input>
	                    <div class="input-group-btn" style="margin: auto">
	                        <button class="btn btn-default" style="" id="submit_searchStudents" type="submit"><span style="margin: 7.5px;" class="glyphicon glyphicon-search"></span></button>
	                    </div>
	                </div>
	            </form>
	            <button class="btn btn-default btn-lg" style="float:right;margin-right: 15px; margin-top: -2px;" onclick="getStudents();"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="margin-top: 2px;"></span></button>
	            <div id="rowsFound_students" style="float:right;margin-top:10px;margin-right:30px;"></div>
	            <div class="table-responsive" style="margin-top: 20px;">
	                <table class="table" id="studentsTable">
	                    
	                </table>
	            </div>
	            
	            
	        </div>
    	</div>
    	
        <script type="text/javascript">
    		
    		$(document).ready(function(){
		        $("#primaryTabs li a[href=\'#section_attendance\']").click();
    		
	    		$("#attendanceDateTimePicker").datetimepicker({
	            	format: "MM/DD/YYYY"
	            });
    			$("#attendanceDateTimePicker").data("DateTimePicker").date(moment());
    			
    			populateAvailableInstructors(null, null, null, function() {loadAttendance()});
		    });
    		
    		function populateAvailableInstructors(successCallback, errorCallback, completeCallback, classesSuccessCallback, classesErrorCallback, classesCompleteCallback) {
    			$.ajax({
    				type: "GET",
    				url: "api/instructor",
    				data: {},
    				dataType: "json",
    				success: [function(data) {
    					$("#attendance_instructor").html("");
    					for (var i = 0; i < data.data.length; i++) {
    						$("#attendance_instructor").append("<option data-value=\'" + JSON.stringify(data.data[i]) + "\'>" + data.data[i].fname + " " + data.data[i].lname + "</option>");
    					}
    				}, function() {
	    				populateAvailableClasses($(\'#attendance_instructor\').find(\':selected\').data(\'value\').a_number, classesSuccessCallback, classesErrorCallback, classesCompleteCallback)
					}, successCallback],
    				error: errorCallback,
    				complete: completeCallback
    			});
    		}
    		
    		function populateAvailableClasses(a_number, successCallback, errorCallback, completeCallback) {
    			$.ajax({
					type: "GET",
    				url: "api/class",
    				data: {
    					a_number: a_number
    				},
    				dataType: "json",
    				success: [function(data) {
    					$("#attendance_class").html("");
    					for (var i = 0; i < data.data.length; i++) {
    						$("#attendance_class").append("<option data-value=\'" + JSON.stringify(data.data[i]) + "\'>" + data.data[i].class_name + "</option>");
    					}
    				}, successCallback],
    				error: errorCallback,
    				complete: completeCallback
				});
    		}
    		
    		//$.ajax({type: "GET", url: "api/admin/", dataType: "json", success: function(data) {loadTable(convertAPIResponseDataToTableFormat(data.data));}});
    		//loadAttendance();
    		
        	function loadAttendance() {
    			show_loader_animation("#reportsTable");
        	    $.ajax({
    				url: "api/class_registration/",
    				data: {
						class_id: $("#attendance_class").find(":selected").data("value").class_id,
    					start_date: {
    						lessThanOrEqualTo: $("#attendanceDateTimePicker").data("DateTimePicker").date().format("YYYY-MM-DD")
    					},
    					end_date: {
    						greaterThanOrEqualTo: $("#attendanceDateTimePicker").data("DateTimePicker").date().format("YYYY-MM-DD")
    					}
					},
    				dataType: "json",
    				success: function(class_registrations) {
    					var n_numbers = [];
    					for (var i = 0; i < class_registrations.data.length; i++) {
    						n_numbers.push(class_registrations.data[i].n_number);
    					}
    					if (n_numbers.length == 0) {
    						n_numbers[0] = "";
    					}
    					$.ajax({
							url: "api/student/",
		    				data: {
								n_number: n_numbers
							},
		    				dataType: "json",
		    				success: function(students) {
    							var registration_ids = [];
    							for (var i = 0; i < class_registrations.data.length; i++) {
		    						registration_ids.push(class_registrations.data[i].registration_id);
		    					}
    							if (registration_ids.length == 0) {
    								registration_ids[0] = "";
    							}
    							$.ajax({
    								url: "api/klockin/",
				    				data: {
										registration_id: registration_ids,
    									date: $("#attendanceDateTimePicker").data("DateTimePicker").date().format("YYYY-MM-DD")
									},
				    				dataType: "json",
				    				success: function(klockins) {
    									var registration_ids_of_those_present = [];
    									for (var i = 0; i < registration_ids.length; i++) {
    										var isPresent = false;
    										var isOnTime = false;
    										var totalTime = 0;
    										for (var k = 0; k < students.data.length; k++) {
    											students.data[k].time_in = null;
    											students.data[k].time_out = null;
    										}
    										for (var j = 0; j < klockins.data.length; j++) {
    											if (class_registrations.data[i].registration_id == klockins.data[j].registration_id) {
    												isPresent = true;
    												if (klockins.data[j].late == "0") {
    													isOnTime = true;
    												}
    												
    												if (klockins.data[j].time_out === null) {
    													totalTime = new Date(0);
    												} else {
    													var time_in = moment(klockins.data[j].time_in, "HH:mm:ss");
    													var time_out = moment(klockins.data[j].time_out, "HH:mm:ss");
    													
    													if (time_in > time_out) {
	    													time_out.add(1, "days");
	    												}
	    												totalTime = moment(time_out - time_in).utc();
    													for (var k = 0; k < students.data.length; k++) {
    														if (class_registrations.data[i].n_number == students.data[k].n_number) {
    															students.data[k].time_in = time_in.format("HH:mm:ss");
    															students.data[k].time_out = time_out.format("HH:mm:ss");
    														}
    													}
    												}
    											}
    										}
    										if (isPresent) {
    											registration_ids_of_those_present.push([class_registrations.data[i].registration_id, isOnTime]);
    										}
    									}
    									
    									var n_numbers_of_those_present = [];
    									for (var i = 0; i < registration_ids_of_those_present.length; i++) {
    										for (var j = 0; j < class_registrations.data.length; j++) {
    											if (registration_ids_of_those_present[i][0] == class_registrations.data[j].registration_id) {
	    											n_numbers_of_those_present.push([class_registrations.data[j].n_number, registration_ids_of_those_present[i][1]]);
	    										}
    										}
    									}
    									
    									var styleclasses = [];
    									for (var i = 0; i < students.data.length; i++) {
    										styleclasses.push(["danger"]);
    										for (var j = 0; j < n_numbers_of_those_present.length; j++) {
    											if (students.data[i].n_number == n_numbers_of_those_present[j][0]) {
    												styleclasses[i].pop();
    												if (n_numbers_of_those_present[j][1]) {
    													styleclasses[i].push("success");
    												} else {
    													styleclasses[i].push("warning");
    												}
	    										}
    										}
    									}
    									
    									loadTable("reportsTable", convertAPIResponseDataToTableFormat(students.data), styleclasses);
    								}
				    			});
		    				}
						});
    				}
				});
        	}
        </script>';
    $headerScripts = '';
    $footerScripts = '';
    $title = 'Reports';
    
    $user = (isset($_SESSION['instructor']) ? $_SESSION['instructor'] : $_SESSION['admin']);
    $requires_temp_authentication = true;
    if ($user->temp_authenticated) {
        $user->temp_authenticated = false;
        (isset($_SESSION['instructor']) ? $_SESSION['instructor'] = $user : $_SESSION['admin'] = $user);
        $requires_temp_authentication = false;
    }
    
    echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$reports_menu_item_id, $title, $requires_temp_authentication);
}
else{
    $content = '<center><h1>Forbidden</h1></center><p>You do not have the permissions to view this page</p>';
    $headerScripts = '';
    $footerScripts = '';
    $title = 'Reports';
    
    echo loader::loadPage($content, $headerScripts, $footerScripts, standard_strings::$reports_menu_item_id, $title);
}
?>
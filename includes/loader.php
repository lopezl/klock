<?php


class loader {
	public static function loadPage($content, $headerScripts, $footerScripts, $activeMenuItemId, $title, $requires_temp_authentication = false) {
		
		$show_klockin_menu_item = false;
		if(isset($_SESSION['instructor'])) {
			$show_klockin_menu_item = true;
		}
		
		$klockin_menu_item = "";
		if ($show_klockin_menu_item) {
			$klockin_menu_item = '<li id="' . strtolower(standard_strings::$klockin_menu_item_id) . '"><a href="klockin.php">' . standard_strings::$klockin_noun . '</a></li>';
		}
		
		if ($requires_temp_authentication) {
			$user = (isset($_SESSION['instructor']) ? $_SESSION['instructor'] : $_SESSION['admin']);
			$content = '<center><h1>' . $title . '</h1></center>
				<center>Please validate your identity</center>
				<form class="form" role="form" autocomplete="off" style="margin-top: 50px;" onsubmit="submit_login(); return false;">
					<div class="alert alert-danger" id="userAlert" style="display: none;"><strong id="userTitle"></strong><p id="userDescription"></p></div>
					<input value="' . (isset($_SESSION['instructor']) ? $user->a_number : $user->username) . '" type="text" name="username" id="username" class="form-control topInput" placeholder="Username" autocomplete="off" readonly>
					<input type="password" name="password" id="password" class="form-control bottomInput" placeholder="Password" required autofocus autocomplete="off">
					<button class="btn btn-lg btn-primary btn-block" id="submitBtn" type="submit">Sign in</button>
					<a href="api/authenticate/logout" style="float:right;">This is not me</a>
				</form>';
			$headerScripts = '';
			$footerScripts = '<script>
			var username = document.getElementById("username");
			var password = document.getElementById("password");
			var userAlert = document.getElementById("userAlert");
			var userTitle = document.getElementById("userTitle");
			var userText = document.getElementById("userDescription");
		
			function submit_login() {
				$.ajax({
					type: "POST",
					url: "api/authenticate/temp",
					data: {"password": password.value},
					dataType: "json",
					success: function(reply) {
						// do different stuff depending on the result returned
						if (reply.data.authenticated) {
							window.location.reload();
						} else {
							userTitle.textContent = "Authentication Error";
							userText.textContent = "' . standard_strings::$incorrect_username_password . '";
							userAlert.style.display = "";
						}
					},
					error: function(reply) {
						userAlertTitle.textContent("System error");
						userAlertText.textContent("backend not responding: " + reply.responseText);
						userAlert.style.display = "";
					}
				});
			}
		  </script>';
		}
		
		return '<!DOCTYPE html>
			<html lang="en">
			  <script type="text/javascript" src="js/jquery.min.js"></script>
			  <script type="text/javascript" src="js/bootstrap.js"></script>
			  <script type="text/javascript" src="js/bootbox.min.js"></script>
			  <script type="text/javascript" src="js/standard.js"></script>
			  <script type="text/javascript" src="js/moment.js"></script>
			  <script type="text/javascript" src="js/manage.js"></script>
			  <script type="text/javascript" src="js/moment-timezone.js"></script>
			  <script type="text/javascript" src="js/bootstrap-timepicker.min.js"></script>
			  ' . $headerScripts . '
			  <head>
				<link rel="icon" href="images/favicon.ico" type="image/icon">
				<!-- Displays the title for the tab -->
				<title>Klock - ' . $title . '</title>
					
				<!-- Imports css and icon -->
				<link href="css/bootstrap.css" rel="stylesheet">
				<!-- <link href="css/bootstrap-theme.css" rel="stylesheet"> -->
				<link href="css/justified-nav.css" rel="stylesheet">
				<link href="css/form.css" rel="stylesheet">
				<link rel="stylesheet" href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css" />
			  </head>
			  
			  <div id="alertContainer"></div>
			  
			  <!-- Body -->
			  <body style="padding-bottom: 80px;">
				<div class="container">
					
				  <div class="masthead">
					
				  <!-- The logo -->
					<img id="logoImg" src="images/klock_with_text.svg" alt="Klock" height="50px" style="margin: 10px;">
					<button class="btn btn-default btn-lg" style="float:right;margin-top:10px;" onclick="$.ajax({url: \'api/authenticate/logout/\', success: function() {window.location.href = \'index.php\';}})"><span class="glyphicon glyphicon-off" aria-hidden="true" style="margin-top: 2px;"></span></button>
					<ul class="nav nav-justified">
					  <!-- The menu -->
					  ' . $klockin_menu_item . '
					  <li id="' . strtolower(standard_strings::$reports_menu_item_id) . '"><a href="reports.php">Reports</a></li>
					  <li id="' . strtolower(standard_strings::$manage_menu_item_id) . '"><a href="manage.php">Manage</a></li>
					  <li id="' . strtolower(standard_strings::$about_menu_item_id) . '"><a href="about.php">About</a></li>
					  <!--<li id="logout_button"><a href="index.php">Log out</a></li>-->
					  
					</ul>
				  </div>
					
				  ' . $content . '
					
				</div>
			  </body>
						
			  ' . self::getFooter() . '
			  <script type="text/javascript">
				// Does the same thing as the jQuery
				document.getElementById("' . strtolower($activeMenuItemId) . '").className += "active";
				
				//$("#' . strtolower($activeMenuItemId) . '").attr("class", "active");
			  </script>
			  ' . $footerScripts . '
			</html>';
	}
	
	public static function getFooter() {
		return '<!-- Site footer -->
		<footer>
	<div class="footer">
	<p>&copy; ' . standard_strings::$copyRightCompanyName . ' ' . date("Y") . '</p>
	</div>
	</footer>';
	}
}
?>